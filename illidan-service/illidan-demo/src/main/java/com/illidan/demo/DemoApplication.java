package com.illidan.demo;

import com.illidan.common.launch.AppName;
import com.illidan.launch.BootApplication;
import com.illidan.launch.IllidanApplication;

/**
 * @author ben
 * @since 2022/5/20
 */
@IllidanApplication
public class DemoApplication {

  public static void main(String[] args) {
    BootApplication.run(AppName.ILLIDAN_DEMO, DemoApplication.class, args);
  }
}
