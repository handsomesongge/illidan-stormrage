package com.illidan.demo.controller;

import cn.hutool.core.lang.Assert;
import com.illidan.core.annotation.ApiLog;
import com.illidan.db.model.AbstractController;
import com.illidan.tool.R;
import com.illidan.tool.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ben
 * @since 2022/5/23
 */
@Slf4j
@RestController
@RequestMapping("/demo")
public class DemoController extends AbstractController {

  @ApiLog
  @GetMapping("test")
  public R<String> test() {
    return data("message");
  }

  @ApiLog
  @GetMapping("test2")
  public R<String> test2() {
    Assert.isTrue(false, () -> new ServiceException("alksdjlaksdjlkasdj"));
    return null;
  }

  @ApiLog
  @GetMapping("test3")
  public void test3() {
    int i = 10 / 0;
  }
}
