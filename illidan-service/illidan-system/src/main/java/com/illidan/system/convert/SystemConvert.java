package com.illidan.system.convert;

import com.illidan.system.entity.SysUser;
import com.illidan.system.vo.SysUserVO;
import org.mapstruct.Mapper;

/**
 * @author ben
 * @since 2022/6/16
 */
@Mapper(componentModel = "spring")
public interface SystemConvert {

  /**
   * 用户转用户视图
   *
   * @param user 用户
   * @return {@link SysUserVO}
   */
  SysUserVO sysUser2SysUserVO(SysUser user);

  /**
   * 用户视图转用户
   *
   * @param userVO 用户视图
   * @return {@link SysUser}
   */
  SysUser sysUserVo2SysUser(SysUserVO userVO);
}
