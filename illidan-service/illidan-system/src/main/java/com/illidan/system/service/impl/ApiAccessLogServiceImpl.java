package com.illidan.system.service.impl;

import com.illidan.core.entity.ApiAccessLog;
import com.illidan.db.model.BaseServiceImpl;
import com.illidan.system.mapper.ApiAccessLogMapper;
import com.illidan.system.service.IApiAccessLogService;
import org.springframework.stereotype.Service;

/**
 * @author ben
 * @since 2022/6/8
 */
@Service
public class ApiAccessLogServiceImpl extends BaseServiceImpl<ApiAccessLogMapper, ApiAccessLog> implements IApiAccessLogService{
}
