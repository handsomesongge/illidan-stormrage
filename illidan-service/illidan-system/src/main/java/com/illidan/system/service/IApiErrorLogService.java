package com.illidan.system.service;

import com.illidan.core.entity.ApiErrorLog;
import com.illidan.db.model.IBaseService;
import com.illidan.system.request.ErrorProcessRequest;

/**
 * @author ben
 * @since 2022/6/8
 */
public interface IApiErrorLogService extends IBaseService<ApiErrorLog> {
  /**
   * api错误日志-处理
   *
   * @param request 请求
   * @return boolean
   */
  boolean process(ErrorProcessRequest request);
}
