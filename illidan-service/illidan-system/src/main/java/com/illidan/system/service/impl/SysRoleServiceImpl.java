package com.illidan.system.service.impl;

import com.illidan.db.model.BaseServiceImpl;
import com.illidan.system.entity.SysRole;
import com.illidan.system.mapper.SysRoleMapper;
import com.illidan.system.service.ISysRoleService;
import org.springframework.stereotype.Service;

/**
 * @author ben
 * @since 2022/6/17
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleMapper, SysRole>
    implements ISysRoleService {

}
