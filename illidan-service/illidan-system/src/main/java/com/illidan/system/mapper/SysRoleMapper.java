package com.illidan.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.illidan.system.entity.SysRole;

import java.util.List;
import java.util.Set;

/**
 * @author ben
 * @since 2022/6/17
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

  /**
   * 通过用户id获取角色列表
   *
   * @param userId 用户id
   * @return {@link List}<{@link SysRole}>
   */
  Set<SysRole> listByUserId(Long userId);
}
