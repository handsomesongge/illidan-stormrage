package com.illidan.system.service;

import com.illidan.core.entity.ApiAccessLog;
import com.illidan.db.model.IBaseService;

/**
 * @author ben
 * @since 2022/6/8
 */
public interface IApiAccessLogService extends IBaseService<ApiAccessLog> {
}
