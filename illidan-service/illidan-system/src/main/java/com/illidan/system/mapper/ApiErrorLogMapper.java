package com.illidan.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.illidan.core.entity.ApiErrorLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ben
 * @since 2022/6/8
 */
@Mapper
public interface ApiErrorLogMapper extends BaseMapper<ApiErrorLog> {
}
