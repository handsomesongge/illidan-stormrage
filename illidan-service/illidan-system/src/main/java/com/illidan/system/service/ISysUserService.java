package com.illidan.system.service;

import com.illidan.db.model.IBaseService;
import com.illidan.system.entity.SysUser;
import com.illidan.system.vo.SysUserVO;

/**
 * @author ben
 * @since 2022/6/16
 */
public interface ISysUserService extends IBaseService<SysUser> {

  /**
   * 用户表-新增
   *
   * @param userVO 用户视图
   * @return boolean
   */
  boolean add(SysUserVO userVO);

  /**
   * 用户表-删除
   *
   * @param userId 用户id
   * @return boolean
   */
  boolean remove(Long userId);

  /**
   * 用户表-修改
   *
   * @param userId 用户id
   * @param userVO 用户视图
   * @return boolean
   */
  boolean modify(Long userId, SysUserVO userVO);

  /**
   * 用户表-详情
   *
   * @param userId 用户id
   * @return {@link SysUserVO}
   */
  SysUserVO info(Long userId);

  /**
   * 根据用户名获取用户
   *
   * @param username 用户名
   * @return {@link SysUser}
   */
  SysUser getUserByUsername(String username);
}
