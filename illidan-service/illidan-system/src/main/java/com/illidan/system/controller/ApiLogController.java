package com.illidan.system.controller;

import cn.hutool.captcha.CaptchaUtil;
import com.illidan.core.entity.ApiAccessLog;
import com.illidan.core.entity.ApiErrorLog;
import com.illidan.db.model.AbstractController;
import com.illidan.db.model.Page;
import com.illidan.db.model.PageQuery;
import com.illidan.system.request.ErrorProcessRequest;
import com.illidan.system.service.IApiAccessLogService;
import com.illidan.system.service.IApiErrorLogService;
import com.illidan.tool.R;
import com.illidan.tool.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 系统日志控制层
 *
 * @author ben
 * @since 2022/6/8
 */
@RestController
@Api(tags = "系统日志控制层")
@RequiredArgsConstructor
@RequestMapping("/log")
public class ApiLogController extends AbstractController {

  private final IApiAccessLogService apiAccessLogService;
  private final IApiErrorLogService apiErrorLogService;

  @PostMapping("access/page")
  @ApiOperation("系统日志-api日志分页")
  public R<Page<ApiAccessLog>> accessPage(@RequestBody PageQuery query) {
    return data(apiAccessLogService.page(query));
  }

  @PostMapping("error/page")
  @ApiOperation("系统日志-api错误日志分页")
  public R<Page<ApiErrorLog>> errorPage(@RequestBody PageQuery query) {
    return data(apiErrorLogService.page(query));
  }

  @PostMapping("error/process")
  @ApiOperation("系统日志-api错误日志处理")
  public Result errorProcess(@Validated @RequestBody ErrorProcessRequest request) {
    return status(apiErrorLogService.process(request));
  }
}
