package com.illidan.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.illidan.system.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ben
 * @since 2022/6/16
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
  /**
   * 根据用户名获取
   *
   * @param username 用户名
   * @return {@link SysUser}
   */
  SysUser getByUsername(String username);
}
