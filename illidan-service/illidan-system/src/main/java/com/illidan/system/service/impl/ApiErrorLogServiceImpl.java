package com.illidan.system.service.impl;

import cn.hutool.core.lang.Assert;
import com.illidan.core.entity.ApiErrorLog;
import com.illidan.db.model.BaseServiceImpl;
import com.illidan.system.mapper.ApiErrorLogMapper;
import com.illidan.system.request.ErrorProcessRequest;
import com.illidan.system.service.IApiErrorLogService;
import com.illidan.tool.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * @author ben
 * @since 2022/6/8
 */
@Service
public class ApiErrorLogServiceImpl extends BaseServiceImpl<ApiErrorLogMapper, ApiErrorLog>
    implements IApiErrorLogService {
  @Override
  @Transactional(rollbackFor = Exception.class)
  public boolean process(ErrorProcessRequest request) {
    ApiErrorLog log = baseMapper.selectById(request.getId());
    Assert.notNull(log, NotFoundException::new);
    log.setProcessTime(LocalDateTime.now());
    log.setProcessRemark(request.getRemark());
    return save(log);
  }
}
