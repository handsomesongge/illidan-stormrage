package com.illidan.system;

import com.illidan.launch.BaseAppName;
import com.illidan.launch.BootApplication;
import com.illidan.launch.IllidanApplication;

/**
 * @author ben
 * @since 2022/6/5
 */
@IllidanApplication
public class SystemApplication {

  public static void main(String[] args) {
    BootApplication.run(BaseAppName.ILLIDAN_SYSTEM, SystemApplication.class, args);
  }
}
