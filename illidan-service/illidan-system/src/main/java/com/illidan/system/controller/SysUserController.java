package com.illidan.system.controller;

import cn.hutool.core.util.StrUtil;
import com.illidan.core.annotation.ApiLog;
import com.illidan.db.model.BaseController;
import com.illidan.db.model.Page;
import com.illidan.db.model.PageQuery;
import com.illidan.system.convert.SystemConvert;
import com.illidan.system.service.ISysUserService;
import com.illidan.system.vo.SysUserVO;
import com.illidan.tool.R;
import com.illidan.tool.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @author ben
 * @since 2022/6/16
 */
@RestController
@Api("用户表控制层")
@RequiredArgsConstructor
@RequestMapping("/user")
public class SysUserController extends BaseController<ISysUserService> {

  private final SystemConvert baseConvert;

  @ApiLog
  @PostMapping("page")
  @ApiOperation("用户表-分页")
  public R<Page<SysUserVO>> page(@RequestBody PageQuery query) {
    return data(baseService.page(query).convert(baseConvert::sysUser2SysUserVO));
  }

  @ApiLog
  @Validated
  @PostMapping
  @ApiOperation("用户表-新增")
  public Result add(@RequestBody SysUserVO userVO) {
    return status(baseService.add(userVO));
  }

  @ApiLog
  @Validated
  @DeleteMapping("{userId}")
  @ApiOperation("用户表-删除")
  @ApiImplicitParam(name = "userId", value = "用户id")
  public Result remove(@NotNull(message = "用户id不能为空") @PathVariable Long userId) {
    return status(baseService.remove(userId));
  }

  @ApiLog
  @Validated
  @PutMapping("{userId}")
  @ApiOperation("用户表-修改")
  @ApiImplicitParams({
    @ApiImplicitParam(name = "userId", value = "用户id", required = true),
    @ApiImplicitParam(name = "userVO", value = "用户视图")
  })
  public Result modify(
      @NotNull(message = "用户id不能为空") @PathVariable Long userId, @RequestBody SysUserVO userVO) {
    return status(baseService.modify(userId, userVO));
  }

  @ApiLog
  @Validated
  @GetMapping("{userId}")
  @ApiOperation("用户表-详情")
  public R<SysUserVO> info(@NotNull(message = "用户id不能为空") @PathVariable Long userId) {
    return data(baseService.info(userId));
  }
}
