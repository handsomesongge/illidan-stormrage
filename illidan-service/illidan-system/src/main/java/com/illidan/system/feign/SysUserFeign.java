package com.illidan.system.feign;

import com.illidan.db.model.BaseController;
import com.illidan.system.entity.SysUser;
import com.illidan.system.service.ISysUserService;
import com.illidan.tool.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ben
 * @since 2022/6/16
 */
@RestController
public class SysUserFeign extends BaseController<ISysUserService> implements ISysUserFeign {
  @Override
  @GetMapping(BASE_API + "/username")
  public R<SysUser> getUserByUsername(String username) {
    return data(baseService.getUserByUsername(username));
  }
}
