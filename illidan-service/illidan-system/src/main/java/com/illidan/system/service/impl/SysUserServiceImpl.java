package com.illidan.system.service.impl;

import cn.hutool.core.lang.Assert;
import com.illidan.db.model.BaseServiceImpl;
import com.illidan.system.convert.SystemConvert;
import com.illidan.system.entity.SysRole;
import com.illidan.system.entity.SysUser;
import com.illidan.system.mapper.SysRoleMapper;
import com.illidan.system.mapper.SysUserMapper;
import com.illidan.system.service.ISysUserService;
import com.illidan.system.vo.SysUserVO;
import com.illidan.tool.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * @author ben
 * @since 2022/6/16
 */
@Service
@RequiredArgsConstructor
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper, SysUser>
    implements ISysUserService {

  private final SystemConvert baseConvert;
  private final SysRoleMapper sysRoleMapper;

  @Override
  @Transactional(rollbackFor = Exception.class)
  public boolean add(SysUserVO userVO) {
    // TODO: 2022/6/16 ben mock数据
    userVO.setPassword("123456");
    return save(baseConvert.sysUserVo2SysUser(userVO));
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public boolean remove(Long userId) {
    return removeById(userId);
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public boolean modify(Long userId, SysUserVO userVO) {
    existedCheck(userId);
    SysUser sysUser = baseConvert.sysUserVo2SysUser(userVO);
    sysUser.setId(userId);
    return updateById(sysUser);
  }

  @Override
  public SysUserVO info(Long userId) {
    SysUser user = existedCheck(userId);
    return baseConvert.sysUser2SysUserVO(user);
  }

  @Override
  public SysUser getUserByUsername(String username) {
    SysUser user = baseMapper.getByUsername(username);
    Assert.notNull(user, () -> new NotFoundException("输入用户名不存在或错误"));
    Set<SysRole> roles = sysRoleMapper.listByUserId(user.getId());
    user.setRoles(roles);
    return user;
  }

  /**
   * 存在检查
   *
   * @param userId 用户id
   * @return {@link SysUser}
   */
  private SysUser existedCheck(Long userId) {
    SysUser user = baseMapper.selectById(userId);
    Assert.notNull(user, () -> new NotFoundException("用户不存在"));
    return user;
  }
}
