package com.illidan.system.feign;

import com.illidan.core.entity.ApiAccessLog;
import com.illidan.core.entity.ApiErrorLog;
import com.illidan.core.feign.IApiLogFeign;
import com.illidan.system.service.IApiAccessLogService;
import com.illidan.system.service.IApiErrorLogService;
import com.illidan.tool.R;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ben
 * @since 2022/6/5
 */
@RestController
@RequiredArgsConstructor
public class ApiLogFeign implements IApiLogFeign {

  private final IApiAccessLogService apiAccessLogService;
  private final IApiErrorLogService apiErrorLogService;

  @Override
  @PostMapping(BASE_API + "/save/accessLog")
  @Transactional(rollbackFor = Exception.class)
  public R<String> saveAccessLog(@RequestBody ApiAccessLog apiAccessLog) {
    return R.status(apiAccessLogService.save(apiAccessLog));
  }

  @Override
  @PostMapping(BASE_API + "/save/errorLog")
  @Transactional(rollbackFor = Exception.class)
  public R<String> saveErrorLog(@RequestBody ApiErrorLog apiErrorLog) {
    return R.status(apiErrorLogService.save(apiErrorLog));
  }

}
