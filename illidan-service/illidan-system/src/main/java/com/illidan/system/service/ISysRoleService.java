package com.illidan.system.service;

import com.illidan.db.model.IBaseService;
import com.illidan.system.entity.SysRole;

/**
 * @author ben
 * @since 2022/6/17
 */
public interface ISysRoleService extends IBaseService<SysRole> {

}
