# Lombok

### 注解

***只有常用的注解***

#### @AllArgsConstructor 类注解

> ##### 作用
生成一个包含所有参数的构造方法
> ##### 参数

字段|作用
---|---
`staticName`|不为空的话，生成一个静态方法返回实例，并把构造器设置为`private`
`access`|构造器访问权限修饰符，默认`public`
`onConstructor`|用于给构造方法添加注解,注意下换线

> ##### 示例

```java
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor_ = {@Bean}, staticName = "create")
public class Example {
    private int foo;
    private final String bar;
}
// Generate
public class Example {
    private int foo;
    private final String bar;
    @Bean
    private Example(int foo, String bar) {
        this.foo = foo;
        this.bar = bar;
    }
    public static Example create(int foo, String bar) {
        return new Example(foo, bar);
    }
}
```

#### @RequiredArgsConstructor 类注解

> ##### 作用
生成类中所有带有@NonNull注解的或者带有final修饰的成员变量生成对应的构造方法
> ##### 参数

字段|作用
---|---
`staticName`|不为空的话，生成一个静态方法返回实例，并把构造器设置为`private`
`access`|构造器访问权限修饰符，默认`public`
`onConstructor`|用于给构造方法添加注解,注意下换线

> ##### 示例

```java
@RequiredArgsConstructor(staticName = "create")
public class Example {
    @NonNull
    private int foo;
    @NonNull
    private final String bar;
    private int head;
}
// Generate
public class Example {
    private int foo;
    private final String bar;
    private int head;
    private Example(@NonNull int foo, @NonNull String bar) {
        this.foo = foo;
        this.bar = bar;
    }
    public static Example create(@NonNull int foo, @NonNull String bar) {
        return new Example(foo, bar);
    }
}
```

#### @NoArgsConstructor 类注解

> ##### 作用
生成一个空构造方法   
*注：如果类中含有`final`修饰的成员变量，是无法使用`@NoArgsConstructor`注解的。*
> ##### 参数

字段|作用
---|---
`staticName`|不为空的话，生成一个静态方法返回实例，并把构造器设置为`private`
`access`|构造器访问权限修饰符，默认`public`
`onConstructor`|用于给构造方法添加注解,注意下换线
`force`|是否给所有`final`字段赋值 `0 / null / false`，防止报错。

> ##### 示例

```java
@AllArgsConstructor(staticName = "create")
public class Example {
    private int foo;
    private String bar;
}
// Generate
public class Example {
    private int foo;
    private String bar;
    private Example() {
    }
    public static Example create(int foo, String bar) {
        return new Example(foo, bar);
    }
}
```

#### @Builder 类注解

> ##### 作用
生成构建者(`Builder`)模式，生成一个用于创建对象的静态方法，更加合适的创建对象方式，推荐使用。
*注：会清空构造方法添加的默认值，可以在字段上添加`@Builder.Default`保证默认值有效*
> ##### 参数

字段|作用
---|---
`builderMethodName`|创建构建器实例的方法名称
`buildMethodName`|构建器类中创建构造器实例的方法名称
`builderClassName`|构造器类名
`toBuilder`|生成`toBuilder()`方法

> ##### 示例

```java
@Builder
public class Example {
    private int foo;
    private final String bar;
}
// Generate
public class Example {
    private int foo;
    private final String bar;
    Example(int foo, String bar) {
        this.foo = foo;
        this.bar = bar;
    }
    public static Example.ExampleBuilder builder() {
        return new Example.ExampleBuilder();
    }
    public static class ExampleBuilder {
        private int foo;
        private String bar;
        ExampleBuilder() {
        }
        public Example.ExampleBuilder foo(int foo) {
            this.foo = foo;
            return this;
        }
        public Example.ExampleBuilder bar(String bar) {
            this.bar = bar;
            return this;
        }
        public Example build() {
            return new Example(this.foo, this.bar);
        }
        public String toString() {
            return "Example.ExampleBuilder(foo=" + this.foo + ", bar=" + this.bar + ")";
        }
        public Example.ExampleBuilder toBuilder() {
            return (new Example.ExampleBuilder()).foo(this.foo).bar(this.bar);
        }
    }
}
```

#### @Cleanup 字段注解

> ##### 作用
在变量上声明`@Cleanup`，生成的代码会把变量用`try{}`包围，并在`finally`块中调用`close()`
> ##### 参数

字段|作用
---|---
`value`|不为空的话，调用其他关闭资源的方法，会包含在`finally`里

> ##### 示例

```java
public class Example {
    public void copyFile(String in, String out) throws IOException {
        @Cleanup 
        FileInputStream inStream = new FileInputStream(in);
        @Cleanup 
        FileOutputStream outStream = new FileOutputStream(out);
        byte[] b = new byte[65536];
        while (true) {
            int r = inStream.read(b);
            if (r == -1) break;
            outStream.write(b, 0, r);
        }
    }
}
// Generate
public class Example {
    public Example() {
    }
    public void copyFile(String in, String out) throws IOException {
        FileInputStream inStream = new FileInputStream(in);
        try {
            FileOutputStream outStream = new FileOutputStream(out);
            try {
                byte[] b = new byte[65536];
                while(true) {
                    int r = inStream.read(b);
                    if (r == -1) {
                        return;
                    }
                    outStream.write(b, 0, r);
                }
            } finally {
                if (Collections.singletonList(outStream).get(0) != null) {
                    outStream.close();
                }
            }
        } finally {
            if (Collections.singletonList(inStream).get(0) != null) {
                inStream.close();
            }
        }
    }
}
```

#### @Data 类注解

> ##### 作用
等同于 `@Getter` + `@Setter`+ `@ToString`+ `@EqualsAndHashCode`+ `@RequiredArgsConstructor`   
*注：@Value注解和@Data类似，区别在于它会把所有成员变量默认定义为private final修饰，并且不会生成set方法。@Value更适合只读性更强的类*
> ##### 参数

字段|作用
---|---
`staticConstructor`|生成静态构造方法

> ##### 示例
***注：代码太长不做演示***

#### @EqualsAndHashCode 类注解

> ##### 作用
生成`toString()`、`equals()`和`hashCode()`方法，还会生成一个`canEqual()`方法，用于判断某个对象是否是当前类的实例。
> ##### 参数

字段|作用
---|---
`callSuper`|是否调用父类的`hashCode()`，默认 `false`
`doNotUseGetters`|是否不调用字段的`getter`，默认如果有`getter`会调用。设置为`true`，直接访问字段，不调用`getter`
`exclude`|此处列出的任何字段都不会在生成的`equals`和`hashCode`中使用。
`of`|与`exclude`相反，设置`of`，`exclude`失效
`onParam`|添加注解，参考`@Getter#onMethod`

> ##### 示例
***注：代码太长不做演示***

#### @Generated 类、字段、方法注解

> ##### 作用
这个注解似乎没有实在的作用，就是标记这个类、字段、方法是自动生成的
> ##### 参数
>##### 示例
***注：无意义注解不做演示***

#### @Getter 类、字段注解

> ##### 作用
生成类所有字段或某个字段的`get()`方法
> ##### 参数
字段|作用
`onMethod`|用于给方法添加注解,注意下换线
`value`|修饰符访问权限
> ##### 示例
***注：注解不做演示***

#### @Setter 类、字段注解

> ##### 作用
生成类所有字段或某个字段的`set()`方法
> ##### 参数
字段|作用
`onMethod`|用于给方法添加注解,注意下换线
`onParam`|用于给方法参数添加注解,注意下换线
`value`|修饰符访问权限
> ##### 示例
***注：注解不做演示***

#### @NonNull 类、字段、方法注解

> ##### 作用
不为空检查，包含类、字段、方法等检测
> ##### 参数
>##### 示例
***注：注解不做演示***

#### @Singular 字段注解

> ##### 作用
这个注解和@Builder一起使用，为Builder生成字段是集合类型的add方法，字段名不能是单数形式，否则需要指定value值
> ##### 参数
>##### 示例

```java
@Builder
public class Example {
    @Singular
    @Setter
    private List<Integer> foos;
}
// Generate
public class Example {
// ...
    public static class ExampleBuilder {
        // 这方法是@Singular作用生成的, 在@Builder基础之上
        public Example.ExampleBuilder foo(Integer foo) {
            if (this.foos == null) {
                this.foos = new ArrayList();
            }

            this.foos.add(foo);
            return this;
        }
    }
// ...
}
```

#### @SneakyThrows 方法注解

> ##### 作用
为方法添加 `try{}catch{}`捕获异常
> ##### 参数
>##### 示例

```java
public class Example {
    @SneakyThrows(UnsupportedEncodingException.class)
    public String utf8ToString(byte[] bytes) {
        return new String(bytes, "UTF-8");
    }
}
// Generate
public class Example {
    public Example() {
    }
    public String utf8ToString(byte[] bytes) {
        try {
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException var3) {
            throw var3;
        }
    }
}
```

#### @Synchronized 方法注解

> ##### 作用
为方法添加 `Synchronized{}`,为代码块加锁
> ##### 参数
>##### 示例

```java
public class Example {
    @Synchronized
    public String utf8ToString(byte[] bytes) {
        return new String(bytes, Charset.defaultCharset());
    }
}
// Generate
public class Example {
    private final Object $lock = new Object[0];
    public Example() {
    }
    public String utf8ToString(byte[] bytes) {
        Object var2 = this.$lock;
        synchronized(this.$lock) {
            return new String(bytes, Charset.defaultCharset());
        }
    }
}
```

#### @val 字段注解

> ##### 作用
字段类型推断注解，字段上使用不需要`@`和字段类型，
*注意：var和val一样，官方文档中说区别就是var不加final修饰，但测试的效果是一样的*
> ##### 参数
>##### 示例

```java
public class ValExample {
    public String example() {
        val example = new ArrayList<String>();
        example.add("Hello, World!");
        val foo = example.get(0);
        return foo.toLowerCase();
    }
    public void example2() {
        val map = new HashMap<Integer, String>();
        map.put(0, "zero");
        map.put(5, "five");
        for (val entry : map.entrySet()) {
            System.out.printf("%d: %s\n", entry.getKey(), entry.getValue());
        }
    }
}
// Generate
public class ValExample {
    public ValExample() {
    }
    public String example() {
        ArrayList<String> example = new ArrayList();
        example.add("Hello, World!");
        String foo = (String)example.get(0);
        return foo.toLowerCase();
    }
    public void example2() {
        HashMap<Integer, String> map = new HashMap();
        map.put(0, "zero");
        map.put(5, "five");
        Iterator var2 = map.entrySet().iterator();
        while(var2.hasNext()) {
            Entry<Integer, String> entry = (Entry)var2.next();
            System.out.printf("%d: %s\n", entry.getKey(), entry.getValue());
        }
    }
}
```

#### @Accessors 类注解

> ##### 作用
默认情况下没有任何作用
> ##### 参数

字段|作用
---|---
`chain`|为`true`时，`setter`链式返回，即`setter`的返回值为`this`
`fluent`|为`true`时，默认设置`chain`为`true`，`setter`的方法名修改为字段名

> ##### 示例

```java
public class Example {
    @Synchronized
    public String utf8ToString(byte[] bytes) {
        return new String(bytes, Charset.defaultCharset());
    }
}
// Generate
public class Example {
    private final Object $lock = new Object[0];
    public Example() {
    }
    public String utf8ToString(byte[] bytes) {
        Object var2 = this.$lock;
        synchronized(this.$lock) {
            return new String(bytes, Charset.defaultCharset());
        }
    }
}
```

#### @Delegate 字段、方法注解

> ##### 作用
代理模式，把字段的方法代理给类，默认代理所有方法
> ##### 参数

字段|作用
---|---
`types`|指定代理的方法
`excludes`|和`types`相反

> ##### 示例

```java
public class Example {
    private interface Add {
        boolean add(String x);
        boolean addAll(Collection<? extends String> x);
    }
    private @Delegate(types = Add.class) List<String> strings;
}
// Generate
public class Example {
    private List<String> strings;
    public Example() {
    }
    public boolean add(String x) {
        return this.strings.add(x);
    }
    public boolean addAll(Collection<? extends String> x) {
        return this.strings.addAll(x);
    }
    private interface Add {
        boolean add(String var1);
        boolean addAll(Collection<? extends String> var1);
    }
}
```

#### @ExtensionMethod 方法注解

> ##### 作用
拓展方法，向现有类型“添加”方法，而无需创建新的派生类型。
> ##### 参数

字段|作用
---|---
`types`|指定代理的方法
`excludes`|和`types`相反

> ##### 示例

```java
@ExtensionMethod({Arrays.class, Extensions.class})
public class Example {
    public static void main(String[] args) {
        int[] intArray = {5, 3, 8, 2};
        intArray.sort();
        int num = 1;
        num = num.increase();
        Arrays.stream(intArray).forEach(System.out::println);
        System.out.println("num = " + num);
    }
}
class Extensions {
    public static int increase(int num) {
        return ++num;
    }
}
// Generate
public class Example {
    public Example() {
    }
    public static void main(String[] args) {
        int[] intArray = new int[]{5, 3, 8, 2};
        Arrays.sort(intArray);
        int num = 1;
        int num = Extensions.increase(num);
        IntStream var10000 = Arrays.stream(intArray);
        PrintStream var10001 = System.out;
        System.out.getClass();
        var10000.forEach(var10001::println);
        System.out.println("num = " + num);
    }
}
```