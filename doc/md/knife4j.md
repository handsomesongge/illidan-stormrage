# Knife4J(Swagger)

### swagger2 和 openApi

> @Api -> @Tag
> @ApiIgnore -> @Parameter(hidden = true) or @Operation(hidden = true) or @Hidden
> @ApiImplicitParam -> @Parameter
> @ApiImplicitParams -> @Parameters
> @ApiModel -> @Schema
> @ApiModelProperty(hidden = true) -> @Schema(accessMode = READ_ONLY)
> @ApiModelProperty -> @Schema
> @ApiOperation(value = "foo", notes = "bar") -> @Operation(summary = "foo", description = "bar")
> @ApiParam -> @Parameter
> @ApiResponse(code = 404, message = "foo") -> @ApiResponse(responseCode = "404", description = "foo")

### 注解

***只有常用的注解***

#### @Api 类注解

> ##### 作用
`Controller` 层注解,用于 `Controller` 说明
> ##### 参数

字段|作用
---|---
`value`|字段说明
`tags`|标签说明
`description`|详情描述
`basePath`|基本路径可以不配置
`position`|如果配置多个 `Api` 想改变显示的顺序位置
`produces`|提供者(`For example, "application/json, application/xml"`)
`consumes`|消费者(`For example, "application/json, application/xml"`)
`protocols`|协议(`Possible values: http, https, ws, wss.`)
`authorizations`|高级特性认证时配置
`hidden`|配置为 `true` 将在文档中隐藏

> ##### 示例

```java
@Api(tags = "HELLO CONTROLLER 测试功能接口")
@RestController
public class HelloController {

}
```

#### @ApiResponses 方法注解

> ##### 作用
`Controller` 层接口注解，表示一组 `response`
> ##### 参数

字段|作用
---|---
`value`|返回说明，`ApiResponse[]` 类型

`ApiResponse` 参数

字段|作用
---|---
`code`|响应的HTTP状态码
`message`|响应的信息内容
`response`|用于描述消息有效负载的可选响应类，对应于响应消息对象的 `schema` 字段
`reference`|指定对响应类型的引用，指定的应用可以使本地引用，也可以是远程引用，将按原样使用，并将覆盖任何指定的`response()`类
`responseHeaders`|声明包装响应的容器，有效值为`List`或`Set`，任何其他值都将被覆盖
`responseContainer`|声明响应的容器，有效值为`List`,`Set`,`Map`，任何其他值都将被忽略
`examples`|例子

> ##### 示例

```java
@ApiResponses(value = {
    @ApiResponse(code = 200, message = "接口返回成功状态"),
    @ApiResponse(code = 500, message = "接口返回未知错误，请联系开发人员调试")
})
@PostMapping("hello")
public Results<UserVO> hello(@RequestBody UserVO userVO){
    Results<UserVO> results = new Results<>(200,"SUCCESS", userVO);
    return results;
}
```

#### @ApiOperation 方法注解

> ##### 作用
Controller层接口注解，用于接口说明，描述接口作用。
> ##### 参数

字段|作用
---|---
`value`|说明接口作用
`notes`|接口的备注说明

> ##### 示例

```java
@ApiOperation(value="用户注册",notes="手机号、密码都是必输项，年龄随边填，但必须是数字")
public Results<UserVO> hello(@RequestBody UserVO userVO){
}
```

#### @ApiImplicitParams 方法注解

> ##### 作用
Controller层接口注解，用于接口参数说明，包含一组 `ApiImplicitParam[]`
> ##### 参数

字段|作用
---|---
`value`|返回说明，`ApiImplicitParam[]` 类型

`ApiImplicitParam` 参数

字段|作用
---|---
`name`|参数名
`value`|参数的汉字说明、解释
`required`|参数是否必须传
`paramType`|参数放在哪个地方
`dataType`|参数类型，默认`String`
`defaultValue`|参数的默认值

*注：`ApiImplicitParam` 的 `paramType` 可以选参数 `header`(参数获取：`@RequestHeader`) 、 `query`(参数获取： `@RequestParam`) 、`path`(参数获取： `@PathVariable`) , `body`(不常用) , `form`(不常用)*

> ##### 示例

```java
@ApiOperation(value="用户登录",notes="随边说点啥")
	@ApiImplicitParams({
		@ApiImplicitParam(name="mobile",value="手机号",required=true,paramType="form"),
		@ApiImplicitParam(name="password",value="密码",required=true,paramType="form"),
		@ApiImplicitParam(name="age",value="年龄",required=true,paramType="form",dataType="Integer")
	})
	@PostMapping("/login")
	public JsonResult login(@RequestParam String mobile, @RequestParam String password,
	@RequestParam Integer age){
		//...
	    return JsonResult.ok(map);
	}
```

#### @ApiModel 类注解

> ##### 作用
实体类上的注解，用于实体类描述
> ##### 参数

字段|作用
---|---
`value`|实体类名称
`description`|实体类描述

> ##### 示例

```java
@ApiModel(value="User对象", description="用户表")
public class User extends BaseEntity {
}
```

#### @ApiModelProperty 字段注解

> ##### 作用
实体类的字段说明注解，用于字段描述
> ##### 参数

字段|作用
---|---
`name`|实体类字段名称
`value`|实体类字段描述
`required`|是否必填
`hidden`|是否隐藏
`example`|举例说明

> ##### 示例

```java
@ApiModel(value="User对象", description="用户表")
public class User extends BaseEntity {
    @ApiModelProperty(value="用户名",name="username",example="xingguo")
    private String username;
}
```