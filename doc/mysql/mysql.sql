CREATE TABLE illidan_api_access_log (
    id               BIGINT AUTO_INCREMENT COMMENT '主键'
        PRIMARY KEY,
    request_id         VARCHAR(64)   DEFAULT ''                NOT NULL COMMENT '请求id',
    user_id          BIGINT        DEFAULT 0                 NOT NULL COMMENT '用户id',
    app_name VARCHAR(50)                             NOT NULL COMMENT '应用名',
    request_method   VARCHAR(16)   DEFAULT ''                NOT NULL COMMENT '请求方法名',
    request_url      VARCHAR(255)  DEFAULT ''                NOT NULL COMMENT '请求地址',
    request_params   VARCHAR(8000) DEFAULT ''                NOT NULL COMMENT '请求参数',
    user_ip          VARCHAR(50)   DEFAULT ''                NOT NULL COMMENT '登录ip',
    user_agent       VARCHAR(200)  DEFAULT ''                NOT NULL COMMENT '登录标识',
    begin_time       DATETIME                                NOT NULL COMMENT '开始请求时间',
    end_time         DATETIME                                NOT NULL COMMENT '结束请求时间',
    duration         INT                                     NOT NULL COMMENT '执行时长',
    result_code      INT           DEFAULT 0                 NOT NULL COMMENT '结果码',
    result_msg       VARCHAR(512)  DEFAULT ''                NULL COMMENT '结果提示',
    deleted          INT           DEFAULT 0                 NOT NULL COMMENT '逻辑删除',
)
    COMMENT '接口访问日志' ENGINE = InnoDB
                     COLLATE = utf8mb4_general_ci;

CREATE TABLE illidan_api_error_log (
    id                           BIGINT AUTO_INCREMENT COMMENT '主键'
        PRIMARY KEY,
    request_id         VARCHAR(64)   DEFAULT ''                NOT NULL COMMENT '请求id',
    user_id                      BIGINT       DEFAULT 0                 NOT NULL COMMENT '用户id',
    app_name             VARCHAR(50)                            NOT NULL COMMENT '应用名',
    request_method               VARCHAR(16)  DEFAULT ''                NOT NULL COMMENT '请求方法名',
    request_url                  VARCHAR(255) DEFAULT ''                NOT NULL COMMENT '请求地址',
    request_params               VARCHAR(255) DEFAULT ''                NOT NULL COMMENT '请求参数',
    user_ip                      VARCHAR(50)  DEFAULT ''                NOT NULL COMMENT '登录ip',
    user_agent                   VARCHAR(200) DEFAULT ''                NOT NULL COMMENT '登录标识',
    exception_time               DATETIME                               NOT NULL COMMENT '异常发生时间',
    exception_name               VARCHAR(128) DEFAULT ''                NOT NULL COMMENT '异常名',
    exception_message            TEXT                                   NOT NULL COMMENT '异常导致的消息',
    exception_root_cause_message TEXT                                   NOT NULL COMMENT '异常导致的根消息',
    exception_stack_trace        TEXT                                   NOT NULL COMMENT '异常的栈轨迹',
    exception_class_name         VARCHAR(512)                           NOT NULL COMMENT '异常发生的类全名',
    exception_file_name          VARCHAR(512)                           NOT NULL COMMENT '异常发生的类文件',
    exception_method_name        VARCHAR(512)                           NOT NULL COMMENT '异常发生的方法名',
    exception_line_number        INT                                    NOT NULL COMMENT '异常发生的方法所在行',
    exception_reason             VARCHAR(100)                           NOT NULL COMMENT '异常发送原因',
    process_status               TINYINT                                NOT NULL COMMENT '处理状态',
    process_time                 DATETIME                               NULL COMMENT '处理时间',
    process_remark varchar(100) default '' not null comment '处理备注',
    deleted                      INT          DEFAULT 0                 NOT NULL COMMENT '逻辑删除',
)
    COMMENT '接口错误日志' ENGINE = InnoDB
                     COLLATE = utf8mb4_general_ci;

CREATE TABLE illidan_auth_log (
    id          BIGINT AUTO_INCREMENT COMMENT '主键'
        PRIMARY KEY,
    request_id         VARCHAR(64)   DEFAULT ''                NOT NULL COMMENT '请求id',
    user_id     BIGINT                                 NOT NULL COMMENT '用户id',
    operation   TINYINT                                NOT NULL COMMENT '操作方式',
    user_ip     VARCHAR(50)  DEFAULT ''                NOT NULL COMMENT '登录ip',
    user_agent  VARCHAR(200) DEFAULT ''                NOT NULL COMMENT '登录标识',
    deleted     INT          DEFAULT 0                 NOT NULL COMMENT '逻辑删除',
    create_time DATETIME     DEFAULT CURRENT_TIMESTAMP NULL COMMENT '创建时间',
    create_id   BIGINT       DEFAULT 0                 NULL COMMENT '创建人'
)
    COMMENT '鉴权日志' ENGINE = InnoDB
                   COLLATE = utf8mb4_general_ci;

CREATE TABLE illidan_dict (
    id          BIGINT AUTO_INCREMENT COMMENT '主键'
        PRIMARY KEY,
    parent_id   BIGINT       DEFAULT 0                 NOT NULL COMMENT '父主键',
    code        VARCHAR(255)                           NOT NULL COMMENT '字典码',
    value       INT                                    NOT NULL COMMENT '字典值',
    name        VARCHAR(255)                           NOT NULL COMMENT '字典名称',
    sort        INT                                    NOT NULL COMMENT '排序',
    remark      VARCHAR(255) DEFAULT ''                NOT NULL COMMENT '字典备注',
    create_id   BIGINT       DEFAULT 1                 NOT NULL COMMENT '创建人',
    create_time DATETIME     DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建时间',
    update_id   BIGINT       DEFAULT 1                 NOT NULL COMMENT '修改人',
    update_time DATETIME     DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    status      INT          DEFAULT 0                 NOT NULL COMMENT '状态',
    version     INT          DEFAULT 0                 NOT NULL COMMENT '乐观锁',
    deleted     TINYINT      DEFAULT 0                 NOT NULL COMMENT '逻辑删除'
)
    COMMENT '字典表' ENGINE = InnoDB
                  COLLATE = utf8mb4_general_ci;

CREATE INDEX idx_code_value
    ON illidan_dict (code, value);

CREATE INDEX idx_parent_id
    ON illidan_dict (parent_id);

CREATE TABLE illidan_menu (
    id          BIGINT AUTO_INCREMENT COMMENT '主键'
        PRIMARY KEY,
    parent_id   BIGINT       DEFAULT 0                 NOT NULL COMMENT '父id',
    title       VARCHAR(10)  DEFAULT ''                NOT NULL COMMENT '标题',
    code        VARCHAR(20)  DEFAULT ''                NOT NULL COMMENT '菜单编码',
    icon        VARCHAR(20)  DEFAULT ''                NOT NULL COMMENT '图标',
    path        VARCHAR(20)  DEFAULT ''                NOT NULL COMMENT '路径',
    type        TINYINT      DEFAULT 0                 NOT NULL COMMENT '类型',
    sort        TINYINT      DEFAULT 0                 NOT NULL COMMENT '排序',
    remark      VARCHAR(255) DEFAULT ''                NOT NULL COMMENT '备注',
    create_id   BIGINT       DEFAULT 1                 NOT NULL COMMENT '创建人',
    create_time DATETIME     DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建时间',
    update_id   BIGINT       DEFAULT 1                 NOT NULL COMMENT '修改人',
    update_time DATETIME     DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    status      TINYINT      DEFAULT 0                 NOT NULL COMMENT '状态',
    version     INT          DEFAULT 0                 NOT NULL COMMENT '乐观锁',
    deleted     TINYINT      DEFAULT 0                 NOT NULL COMMENT '逻辑删除'
)
    COMMENT '菜单表' ENGINE = InnoDB
                  COLLATE = utf8mb4_general_ci;

CREATE TABLE illidan_role (
    id          BIGINT AUTO_INCREMENT COMMENT '主键'
        PRIMARY KEY,
    parent_id   BIGINT      DEFAULT 0                 NOT NULL COMMENT '父主键',
    name        VARCHAR(20) DEFAULT ''                NOT NULL COMMENT '名称',
    code        VARCHAR(30) DEFAULT ''                NOT NULL COMMENT '编码',
    create_id   BIGINT      DEFAULT 1                 NOT NULL COMMENT '创建人',
    create_time DATETIME    DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建时间',
    update_id   BIGINT      DEFAULT 1                 NOT NULL COMMENT '修改人',
    update_time DATETIME    DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    status      TINYINT     DEFAULT 0                 NOT NULL COMMENT '状态',
    deleted     TINYINT     DEFAULT 0                 NOT NULL COMMENT '逻辑删除',
    version     INT         DEFAULT 0                 NOT NULL COMMENT '乐观锁'
)
    COMMENT '角色表' ENGINE = InnoDB
                  COLLATE = utf8mb4_general_ci;

CREATE TABLE illidan_role_menu (
    role_id BIGINT NOT NULL COMMENT '角色id',
    menu_id BIGINT NOT NULL COMMENT '菜单id',
    PRIMARY KEY (role_id, menu_id)
)
    COMMENT '角色菜单关联表' ENGINE = InnoDB
                      COLLATE = utf8mb4_general_ci;

CREATE TABLE illidan_user (
    id          BIGINT AUTO_INCREMENT COMMENT '主键'
        PRIMARY KEY,
    nickname    VARCHAR(20)  DEFAULT ''                NOT NULL COMMENT '昵称',
    username    VARCHAR(20)  DEFAULT ''                NOT NULL COMMENT '用户名',
    password    VARCHAR(100) DEFAULT ''                NOT NULL COMMENT '密码',
    mobile      VARCHAR(20)  DEFAULT ''                NOT NULL COMMENT '手机号',
    email       VARCHAR(100) DEFAULT ''                NOT NULL COMMENT '邮箱',
    remark      VARCHAR(100) DEFAULT ''                NOT NULL COMMENT '备注',
    create_id   BIGINT       DEFAULT 1                 NOT NULL COMMENT '创建人',
    create_time DATETIME     DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建时间',
    update_id   BIGINT       DEFAULT 1                 NOT NULL COMMENT '修改人',
    update_time DATETIME     DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    status      TINYINT      DEFAULT 0                 NOT NULL COMMENT '状态',
    deleted     TINYINT      DEFAULT 0                 NOT NULL COMMENT '逻辑删除',
    version     INT          DEFAULT 0                 NOT NULL COMMENT '乐观锁'
)
    COMMENT '用户表' ENGINE = InnoDB
                  COLLATE = utf8mb4_general_ci;

CREATE TABLE illidan_user_role (
    user_id BIGINT NOT NULL COMMENT '用户id',
    role_id BIGINT NOT NULL COMMENT '角色id',
    PRIMARY KEY (user_id, role_id)
)
    COMMENT '用户角色关联表' ENGINE = InnoDB
                      COLLATE = utf8mb4_general_ci;

