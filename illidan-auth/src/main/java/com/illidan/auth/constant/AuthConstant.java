package com.illidan.auth.constant;

/**
 * @author ben
 * @since 2022/6/20
 */
public interface AuthConstant {

    /**
     * 黑名单token前缀
     */
    String TOKEN_BLACKLIST_PREFIX = "auth:token:blacklist:";
}
