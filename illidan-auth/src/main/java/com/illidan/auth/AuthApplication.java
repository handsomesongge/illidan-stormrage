package com.illidan.auth;

import com.illidan.launch.BaseAppName;
import com.illidan.launch.BootApplication;
import com.illidan.launch.IllidanApplication;

/**
 * @author ben
 * @since 2022/6/16
 */
@IllidanApplication
public class AuthApplication {
  public static void main(String[] args) {
    BootApplication.run(BaseAppName.ILLIDAN_AUTH, AuthApplication.class, args);
  }
}
