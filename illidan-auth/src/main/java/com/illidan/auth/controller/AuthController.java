package com.illidan.auth.controller;

import com.illidan.auth.model.LoginRequest;
import com.illidan.db.model.AbstractController;
import com.illidan.tool.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ben
 * @since 2022/6/28
 */
@RestController
@Api(tags = "认证控制层")
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController extends AbstractController {

  @ApiOperation("登录")
  @PostMapping("login")
  public Result login(@Validated @RequestBody LoginRequest request) {
    return data(request);
  }
}
