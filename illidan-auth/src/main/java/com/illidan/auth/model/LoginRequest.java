package com.illidan.auth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author ben
 * @since 2022/6/29
 */
@Data
@ApiModel("密码登录请求")
public class LoginRequest {

  @ApiModelProperty("授权类型")
  private String grantType;

  @ApiModelProperty("用户名")
  @NotBlank(message = "用户名不能为空")
  @Length(min = 6, max = 20, message = "用户名格式错误")
  @Pattern(regexp = "^[A-Za-z\\d]+$", message = "用户名格式为数字以及字母")
  private String username;

  @ApiModelProperty("密码")
  @NotBlank(message = "密码不能为空")
  @Length(min = 6, max = 20, message = "密码格式错误")
  private String password;

  @ApiModelProperty("验证码")
  @NotBlank(message = "验证码不能为空", groups = CaptchaValidGroup.class)
  private String captcha;

  @ApiModelProperty("uuid")
  @NotBlank(message = "唯一标识不能为空", groups = CaptchaValidGroup.class)
  private String uuid;

  /** 验证码校验组 */
  public interface CaptchaValidGroup {}
}
