package com.illidan.tool;

/**
 * 参数校验异常
 *
 * @author ben
 * @since 2022/6/20
 */
public class ParamValidException extends ServiceException {
  public ParamValidException() {
    setResultCodeEnum(ResultCodeEnum.R_PARAM_VALID);
  }

  public ParamValidException(String message) {
    super(message);
    setResultCodeEnum(ResultCodeEnum.R_PARAM_VALID);
  }

  public ParamValidException(String message, Throwable cause) {
    super(message, cause);
    setResultCodeEnum(ResultCodeEnum.R_PARAM_VALID);
  }

  public ParamValidException(Throwable cause) {
    super(cause);
    setResultCodeEnum(ResultCodeEnum.R_PARAM_VALID);
  }

  protected ParamValidException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    setResultCodeEnum(ResultCodeEnum.R_PARAM_VALID);
  }
}
