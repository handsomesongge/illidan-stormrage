package com.illidan.tool;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.stream.StreamUtil;
import cn.hutool.core.util.ObjectUtil;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 工具包
 *
 * @author ben
 * @since 2022/5/17
 */
public class $ {


  /**
   * 装对象转为{@link Integer}
   *
   * @param o o
   * @return {@link Integer}
   */
  public static Integer toInt(Object o) {
    if (ObjectUtil.isNull(o)) {
      return null;
    }
    try {
      return Integer.parseInt(o.toString());
    } catch (NumberFormatException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  /**
   * 集合转map
   *
   * @param collection 集合
   * @param keyMapper 键映射器
   * @param valueMapper 值映射器
   * @return {@link Map}<{@link K}, {@link V}>
   */
  public static <T, K, V> Map<K, V> toMap(
      Collection<T> collection,
      Function<T, ? extends K> keyMapper,
      Function<T, ? extends V> valueMapper) {
    if (CollUtil.isEmpty(collection)) {
      return MapUtil.empty();
    }
    return StreamUtil.of(collection).collect(Collectors.toMap(keyMapper, valueMapper));
  }

  /**
   * 集合转map
   *
   * @param collection 集合
   * @param keyMapper 键映射器
   * @return {@link Map}<{@link K}, {@link T}>
   */
  public static <T, K> Map<K, T> toMap(
      Collection<T> collection, Function<T, ? extends K> keyMapper) {
    return toMap(collection, keyMapper, Function.identity());
  }

  /**
   * 分组
   *
   * @param collection 集合
   * @param classifier 分类器
   * @return {@link Map}<{@link K}, {@link List}<{@link T}>>
   */
  public static <T, K> Map<K, List<T>> groupingBy(
      Collection<T> collection, Function<? super T, ? extends K> classifier) {
    if (CollUtil.isEmpty(collection)) {
      return MapUtil.empty();
    }
    return StreamUtil.of(collection).collect(Collectors.groupingBy(classifier));
  }
}
