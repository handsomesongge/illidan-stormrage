package com.illidan.tool;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 响应状态码
 *
 * @author ben
 * @since 2022/5/17
 */
@Getter
@AllArgsConstructor
public enum ResultCodeEnum {

  /** 操作成功 */
  R_SUCCESS("操作成功", 200),
  /** 服务异常 */
  R_SERVER_FAILED("服务异常", 500),
  /** 操作失败 */
  R_BUSINESS_FAILURE("操作失败", 500),
  /** 请求不存在 */
  R_NOT_FOUND("请求不存在", 404),
  /** 请求被拒绝 */
  R_REQ_REJECT("请求被拒绝", 403),
  /** 请求未授权 */
  R_UNAUTHORIZED("请求未授权", 401),
  /** 请求无权限 */
  R_NO_PERMISSION("请求无权限", 403),
  /** 请求记录不存在 */
  R_RECORD_NOT_FOUND("请求记录不存在", 5004),
  /** 参数校验失败 */
  R_PARAM_VALID("参数校验失败", 5003);

  /** 消息 */
  private final String msg;
  /** 代码 */
  private final Integer code;
}
