package com.illidan.tool;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.ObjectUtil;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 树形结构工具
 *
 * @author ben
 * @since 2022/5/17
 */
public class TreeUtil {

  /**
   * 组合树形结构
   *
   * @param nodes 节点
   * @return {@link List}<{@link TreeNode}>
   */
  public static List<TreeNode> merge(List<TreeNode> nodes) {
    return getRootNodes(nodes).stream()
        .peek(node -> node.setChildren(buildChildrenNodes(node.getId(), nodes)))
        .collect(Collectors.toList());
  }

  /**
   * 组合子节点
   *
   * @param id 父id
   * @param nodes 节点
   * @return {@link List}<{@link TreeNode}>
   */
  private static List<TreeNode> buildChildrenNodes(Long id, List<TreeNode> nodes) {
    return nodes.stream()
        .filter(node -> node.getParentId().equals(id))
        .peek(node -> node.setChildren(buildChildrenNodes(node.getId(), nodes)))
        .collect(Collectors.toList());
  }

  /**
   * 获取所有根节点
   *
   * @param nodes 节点
   * @return {@link List}<{@link TreeNode}>
   */
  private static <T extends TreeNode> List<T> getRootNodes(List<T> nodes) {
    return nodes.stream().filter(node -> node.getParentId() == 0).collect(Collectors.toList());
  }

  /**
   * 解析树形结构，生成一维数组
   *
   * @param nodes 节点
   * @return {@link List}<{@link TreeNode}>
   */
  public static List<TreeNode> resolveTree2List(List<TreeNode> nodes) {
    List<TreeNode> result = ListUtil.list(false);
    IntStream.range(0, nodes.size())
        .forEach(
            index ->
                result.addAll(
                    resolveChildrenNode(nodes.get(index), nodes.get(index).getParentId(), index)));
    return result;
  }

  /**
   * 递归解析子节点
   *
   * @param node 节点
   * @param index 索引
   * @return {@link List}<{@link TreeNode}>
   */
  private static <T extends TreeNode> List<TreeNode> resolveChildrenNode(
      T node, Long parentId, int index) {
    List<TreeNode> result = ListUtil.list(false);
    if (ObjectUtil.isNotNull(node)) {
      node.setSort(index);
      node.setParentId(parentId);
      result.add(node);
      List<? extends TreeNode> childrenNodes = node.getChildren();
      if (CollUtil.isNotEmpty(childrenNodes)) {
        IntStream.range(0, childrenNodes.size())
            .forEach(
                i -> result.addAll(resolveChildrenNode(childrenNodes.get(i), node.getId(), i)));
        node.setChildren(ListUtil.empty());
      }
    }
    return result;
  }
}
