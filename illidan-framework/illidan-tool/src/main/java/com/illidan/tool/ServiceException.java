package com.illidan.tool;

import lombok.Data;

/**
 * 基础异常
 *
 * @author ben
 * @since 2022/5/17
 */
@Data
public class ServiceException extends RuntimeException {

  protected static final long serialVersionUID = 1646804679842248728L;

  protected ResultCodeEnum resultCodeEnum;

  public ServiceException() {
    super();
    this.resultCodeEnum = ResultCodeEnum.R_SERVER_FAILED;
  }

  public ServiceException(String message) {
    super(message);
    this.resultCodeEnum = ResultCodeEnum.R_SERVER_FAILED;
  }

  public ServiceException(String message, Throwable cause) {
    super(message, cause);
    this.resultCodeEnum = ResultCodeEnum.R_SERVER_FAILED;
  }

  public ServiceException(Throwable cause) {
    super(cause);
    this.resultCodeEnum = ResultCodeEnum.R_SERVER_FAILED;
  }

  protected ServiceException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    this.resultCodeEnum = ResultCodeEnum.R_SERVER_FAILED;
  }

  public Integer getResultCode() {
    return this.resultCodeEnum.getCode();
  }
}
