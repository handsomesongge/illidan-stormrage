package com.illidan.tool;

import lombok.Getter;

/**
 * @author ben
 * @since 2022/6/8
 */
@Getter
public class NotFoundException extends ServiceException {
  public NotFoundException() {
    super("查询记录不存在");
    this.resultCodeEnum = ResultCodeEnum.R_RECORD_NOT_FOUND;
  }

  public NotFoundException(String message) {
    super(message);
    this.resultCodeEnum = ResultCodeEnum.R_RECORD_NOT_FOUND;
  }

  public NotFoundException(String message, Throwable cause) {
    super(message, cause);
    this.resultCodeEnum = ResultCodeEnum.R_RECORD_NOT_FOUND;
  }

  public NotFoundException(Throwable cause) {
    super(cause);
    this.resultCodeEnum = ResultCodeEnum.R_RECORD_NOT_FOUND;
  }

  protected NotFoundException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    this.resultCodeEnum = ResultCodeEnum.R_RECORD_NOT_FOUND;
  }
}
