package com.illidan.tool;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.SuperBuilder;

/**
 * @author ben
 * @since 2022/5/17
 */
@Data
@SuperBuilder
@ApiModel("字符串树形节点")
public class StringTreeNode extends TreeNode {

  @ApiModelProperty("字符串")
  private String str;
}
