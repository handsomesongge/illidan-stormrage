package com.illidan.tool;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

/**
 * 标准返回体
 *
 * @author ben
 * @since 2022/5/16
 */
@Data
@Builder
@Accessors(chain = true)
@ApiModel("标准返回体")
public class R<T> implements Result{

  @ApiModelProperty("消息")
  private String message;

  @ApiModelProperty("状态码")
  private Integer code;

  @ApiModelProperty("承载数据")
  private T data;

  /**
   * 判断是否成功
   *
   * @return boolean
   */
  public boolean isSuccess() {
    return ResultCodeEnum.R_SUCCESS.getCode().equals(code);
  }

  /**
   * 转map
   *
   * @return {@link Map}<{@link String}, {@link Object}>
   */
  public Map<String, Object> toMap() {
    Map<String, Object> map = new HashMap<>(4);
    map.put("message", message);
    map.put("code", code);
    map.put("data", data);
    map.put("success", false);
    return map;
  }

  // 常规返回体方法

  @SuppressWarnings("unchecked")
  public static <T> R<T> code(ResultCodeEnum resultCodeEnum) {
    return code(resultCodeEnum, (T) "暂无承载数据");
  }

  @SuppressWarnings("unchecked")
  public static <T> R<T> code(ResultCodeEnum resultCodeEnum, String message) {
    return R.<T>builder()
        .code(resultCodeEnum.getCode())
        .message(message)
        .data((T) "暂无承载数据")
        .build();
  }

  public static <T> R<T> code(ResultCodeEnum resultCodeEnum, T data) {
    return R.<T>builder()
        .code(resultCodeEnum.getCode())
        .message(resultCodeEnum.getMsg())
        .data(data)
        .build();
  }

  public static <T> R<T> ok() {
    return R.code(ResultCodeEnum.R_SUCCESS);
  }

  public static <T> R<T> ok(String message) {
    return R.<T>ok().setMessage(message);
  }

  public static <T> R<T> data(T data) {
    return R.<T>ok().setData(data);
  }

  public static <T> R<T> data(T data, String message) {
    return R.<T>ok().setMessage(message).setData(data);
  }

  public static <T> R<T> fail() {
    return R.code(ResultCodeEnum.R_BUSINESS_FAILURE);
  }

  public static <T> R<T> fail(String message) {
    return R.<T>fail().setMessage(message);
  }

  public static <T> R<T> fail(Integer code, String message) {
    return R.<T>fail(message).setCode(code);
  }

  public static <T> R<T> status(boolean condition) {
    return condition ? ok() : fail();
  }
}
