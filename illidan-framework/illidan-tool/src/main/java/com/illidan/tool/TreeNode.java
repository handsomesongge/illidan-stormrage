package com.illidan.tool;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * @author ben
 * @since 2022/5/17
 */
@Data
@SuperBuilder
@ApiModel("树形结构")
public class TreeNode implements Serializable {

  @ApiModelProperty("主键")
  private Long id;

  @ApiModelProperty("父主键")
  private Long parentId;

  @ApiModelProperty("排序")
  private Integer sort;

  @ApiModelProperty("子节点")
  private List<TreeNode> children;
}
