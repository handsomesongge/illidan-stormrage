package com.illidan.cloud.interceptor;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.util.Set;

/**
 * okhttp 日志拦截器
 *
 * @author ben
 * @since 2022/6/15
 */
@Slf4j
public class OkHttpLogInterceptor implements Interceptor {

  public static OkHttpLogInterceptor of() {
    return new OkHttpLogInterceptor();
  }

  @Override
  public Response intercept(Chain chain) throws IOException {
    Request request = chain.request();
    long beginTime = System.currentTimeMillis();
    formatRequestLog(request);
    Response response = chain.proceed(request);
    formatResponseLog(response, beginTime);
    return response;
  }

  /**
   * 请求日志
   *
   * <pre>
   * -------------------- request begin --------------------
   * request-id 83bb00ac-0f1a-4b46-94b0-c050fcc625be
   * ==> GET http://localhsot:8080/
   * [header]
   * content-type = application/json
   * ...
   * [params]
   * key = value
   * [body] // 如果是post请求
   * -------------------- request end --------------------
   * </pre>
   *
   * @param request 请求
   */
  @SneakyThrows
  private void formatRequestLog(Request request) {
    StringBuilder logBuilder = StrUtil.builder(200);
    logBuilder.append("\n-------------------- request begin --------------------\n");
    HttpUrl url = request.url();
    logBuilder
        .append("request-id ")
        .append(request.header("requestId"))
        .append("==> ")
        .append(request.method())
        .append(url)
        .append(StrUtil.LF);
    Headers headers = request.headers();
    Set<String> headerNames = headers.names();
    if (CollUtil.isNotEmpty(headerNames)) {
      logBuilder.append("[header]\n");
      headerNames.forEach(
          name ->
              logBuilder.append(name).append(" = ").append(headers.get(name)).append(StrUtil.LF));
    }
    Set<String> parameterNames = url.queryParameterNames();
    if (CollUtil.isNotEmpty(parameterNames)) {
      logBuilder.append("[params]\n");
      parameterNames.forEach(
          name ->
              logBuilder
                  .append(name)
                  .append(" = ")
                  .append(url.queryParameter(name))
                  .append(StrUtil.LF));
    }
    if (HttpMethod.POST.equals(request.method()) && request.body().contentLength() > 0) {
      logBuilder.append("[body]\n").append(request.body().toString());
    }
    logBuilder.append("\n-------------------- request end --------------------\n");
    log.info(logBuilder.toString());
  }

  /**
   * 日志格式
   *
   * <pre>
   * -------------------- response begin --------------------
   * request-id 83bb00ac-0f1a-4b46-94b0-c050fcc625be
   * [result]
   * R json
   * <== GET http://localhsost:8080/ (100 ms)
   * -------------------- response end --------------------
   * </pre>
   *
   * @param response 响应
   * @param beginTime 开始时间
   */
  private void formatResponseLog(Response response, long beginTime) throws IOException {
    StringBuilder logBuilder = StrUtil.builder(200);
    logBuilder.append("\n-------------------- response begin --------------------\n");
    logBuilder
        .append("request-id ")
        .append(response.header("requestId"))
        .append(StrUtil.LF)
        .append("[result]\n")
        .append(response.body().string())
        .append("\n<== ")
        .append(response.request().method())
        .append(response.request().url())
        .append("(")
        .append(System.currentTimeMillis() - beginTime)
        .append(" ms)\n");
    logBuilder.append("\n-------------------- response end --------------------\n");
    log.info(logBuilder.toString());
  }
}
