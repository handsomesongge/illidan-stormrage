//package com.illidan.cloud.config;
//
//import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.retry.interceptor.RetryInterceptorBuilder;
//import org.springframework.retry.interceptor.RetryOperationsInterceptor;
//
///**
// * @author ben
// * @since 2022/6/10
// */
//@Configuration(proxyBeanMethods = false)
//public class RetryConfig {
//    @Bean
//    @ConditionalOnMissingBean(name = "configServerRetryInterceptor")
//    public RetryOperationsInterceptor configServerRetryInterceptor() {
//        return RetryInterceptorBuilder
//                .stateless()
//                .backOffOptions(1000, 1.2, 5000)
//                .maxAttempts(10)
//                .build();
//    }
//
//}
