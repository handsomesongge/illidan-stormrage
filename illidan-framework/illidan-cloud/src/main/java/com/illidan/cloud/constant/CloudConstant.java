package com.illidan.cloud.constant;

/**
 * @author ben
 * @since 2022/5/20
 */
public interface CloudConstant {

  /** feign基础路径 */
  String FEIGN_BASE_API = "/client";
}
