//package com.illidan.cloud.config;
//
//import com.illidan.cloud.interceptor.OkHttpLogInterceptor;
//import feign.Contract;
//import feign.RequestInterceptor;
//import feign.Retryer;
//import okhttp3.OkHttpClient;
//import org.springframework.cloud.openfeign.support.SpringMvcContract;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Enumeration;
//import java.util.Objects;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author ben
// * @since 2022/6/15
// */
//@Configuration(proxyBeanMethods = false)
//public class FeignConfig {
//
//  @Bean
//  public OkHttpClient okHttpClient() {
//    OkHttpClient.Builder builder = new OkHttpClient.Builder();
//    return builder
//        .connectTimeout(5, TimeUnit.SECONDS)
//        .readTimeout(5, TimeUnit.SECONDS)
//        .writeTimeout(5, TimeUnit.SECONDS)
//        .retryOnConnectionFailure(true)
//        .addInterceptor(OkHttpLogInterceptor.of())
//        .build();
//  }
//
//  @Bean
//  public RequestInterceptor requestInterceptor() {
//    return template -> {
//      ServletRequestAttributes requestAttributes =
//          (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//      HttpServletRequest request = Objects.requireNonNull(requestAttributes).getRequest();
//      Enumeration<String> headerNames = request.getHeaderNames();
//      while (headerNames.hasMoreElements()) {
//        String headerName = headerNames.nextElement();
//        template.header(headerName, request.getHeader(headerName));
//      }
//    };
//  }
//
//  @Bean
//  public Retryer retryer() {
//    return new Retryer.Default();
//  }
//
//  @Bean
//  public Contract contract() {
//    return new SpringMvcContract();
//  }
//}
