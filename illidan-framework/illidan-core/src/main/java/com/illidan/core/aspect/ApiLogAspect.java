package com.illidan.core.aspect;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.illidan.core.annotation.ApiLog;
import com.illidan.core.publisher.ApiLogPublisher;
import com.illidan.tool.R;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * @author ben
 * @since 2022/5/20
 */
@Slf4j
@Aspect
@Order(2)
public class ApiLogAspect {

  @Pointcut("@annotation(com.illidan.core.annotation.ApiLog)")
  public void pointcut() {}

  @SneakyThrows
  @Around("pointcut()")
  public Object apiLogAround(ProceedingJoinPoint pjp) {
    ApiLog apiLog = ((MethodSignature) pjp.getSignature()).getMethod().getAnnotation(ApiLog.class);
    if (!apiLog.enable()) {
      return pjp.proceed();
    }
    LocalDateTime beginTime = LocalDateTime.now();
    Object result;
    result = pjp.proceed();
    LocalDateTime endTime = LocalDateTime.now();
    long duration = LocalDateTimeUtil.between(beginTime, endTime, ChronoUnit.MILLIS);
    if (result instanceof R<?>) {
      ApiLogPublisher.publishEvent(beginTime, endTime, duration, (R<?>) result);
    }
    return result;
  }
}
