package com.illidan.core.config;

import com.illidan.core.publisher.ApiErrorLogPublisher;
import com.illidan.tool.R;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.lang.Nullable;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

/**
 * @author ben
 * @since 2022/6/9
 */
public class BaseErrorAttributes extends DefaultErrorAttributes {

  public static BaseErrorAttributes of() {
    return new BaseErrorAttributes();
  }

  @Override
  public Map<String, Object> getErrorAttributes(
      WebRequest webRequest, ErrorAttributeOptions options) {
    // 获取状态码
    Integer status = getAttribute(webRequest, "javax.servlet.error.status_code");
    // 获取异常
    Throwable error = getError(webRequest);
    String message = "未知异常:" + status;
    R<Object> result = error == null ? R.fail(message) : R.fail(status, error.getMessage());
    ApiErrorLogPublisher.publishEvent(error, message, result);
    return result.toMap();
  }

  /**
   * 获取属性,{@link DefaultErrorAttributes#getAttribute(RequestAttributes, String)}
   *
   * @param webRequest web请求
   * @param name 名字
   * @return {@link T}
   */
  @Nullable
  @SuppressWarnings("unchecked")
  public <T> T getAttribute(WebRequest webRequest, String name) {
    return (T) webRequest.getAttribute(name, RequestAttributes.SCOPE_REQUEST);
  }
}
