package com.illidan.core.event;

import com.illidan.core.entity.ApiAccessLog;
import org.springframework.context.ApplicationEvent;

/**
 * @author ben
 * @since 2022/5/23
 */
public class ApiLogEvent extends ApplicationEvent {

  private static final long serialVersionUID = -567724978454618231L;

  public ApiLogEvent(ApiAccessLog source) {
    super(source);
  }

  public static ApiLogEvent of(ApiAccessLog log) {
    return new ApiLogEvent(log);
  }

  public ApiAccessLog getLog() {
    return ((ApiAccessLog) this.getSource());
  }
}
