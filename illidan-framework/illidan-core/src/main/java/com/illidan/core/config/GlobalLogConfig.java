package com.illidan.core.config;

import com.illidan.core.aspect.ApiLogAspect;
import com.illidan.core.feign.IApiLogFeign;
import com.illidan.core.aspect.RequestLogAspect;
import com.illidan.core.listener.ApiErrorLogListener;
import com.illidan.core.listener.ApiLogListener;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ben
 * @since 2022/6/15
 */
@RequiredArgsConstructor
@ConditionalOnWebApplication
@Configuration(proxyBeanMethods = false)
public class GlobalLogConfig {
  private final IApiLogFeign apiLogFeign;

  @Bean
  public ApiLogAspect apiLogAspect() {
    return new ApiLogAspect();
  }

  @Bean
  public RequestLogAspect requestLogAspect() {
    return new RequestLogAspect();
  }

  @Bean
  public ApiErrorLogListener apiErrorLogListener() {
    return new ApiErrorLogListener(apiLogFeign);
  }

  @Bean
  public ApiLogListener apiLogListener() {
    return new ApiLogListener(apiLogFeign);
  }
}
