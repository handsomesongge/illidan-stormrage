package com.illidan.core.config;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author ben
 * @since 2022/6/15
 */
public class RequestBodyWrapper extends HttpServletRequestWrapper {
  private final String body;

  public RequestBodyWrapper(HttpServletRequest request) throws IOException {
    super(request);
    StringBuilder stringBuilder = new StringBuilder();
    String line;
    BufferedReader reader = request.getReader();
    while ((line = reader.readLine()) != null) {
      stringBuilder.append(line);
    }
    body = stringBuilder.toString();
  }

  @Override
  public ServletInputStream getInputStream() throws IOException {
    final ByteArrayInputStream bais = new ByteArrayInputStream(body.getBytes());
    return new ServletInputStream() {
      @Override
      public boolean isFinished() {
        return false;
      }
      @Override
      public boolean isReady() {
        return false;
      }
      @Override
      public void setReadListener(ReadListener readListener) {
      }

      @Override
      public int read() {
        return bais.read();
      }
    };
  }

  public String getBody() {
    return body;
  }


  @Override
  public BufferedReader getReader() throws IOException {
    return new BufferedReader(new InputStreamReader(this.getInputStream()));
  }
}
