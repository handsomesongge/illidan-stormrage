package com.illidan.core.listener;

import com.illidan.core.event.ApiErrorLogEvent;
import com.illidan.core.feign.IApiLogFeign;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;

/**
 * @author ben
 * @since 2022/5/27
 */
@Slf4j
@RequiredArgsConstructor
public class ApiErrorLogListener {

  private final IApiLogFeign apiLogFeign;

  @EventListener({ApiErrorLogEvent.class})
  public void appErrorLog(ApiErrorLogEvent event) {
    try {
      apiLogFeign.saveErrorLog(event.getLog());
    } catch (Exception e) {
      log.error("api error log insert failed:[{}]", e.getMessage());
      e.printStackTrace();
    }
  }
}
