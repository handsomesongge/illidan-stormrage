package com.illidan.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.illidan.db.annotation.BindDict;
import com.illidan.db.model.PrimaryKeyEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

/**
 * @author ben
 * @since 2022/5/20
 */
@Data
@SuperBuilder
@NoArgsConstructor
@TableName("illidan_api_access_log")
public class ApiAccessLog extends PrimaryKeyEntity {

  /** 应用名 */
  private String appName;
  /** 请求主键 */
  private String requestId;
  /** 请求方法名 */
  private String requestMethod;
  /** 请求地址 */
  private String requestUrl;
  /** 请求体 */
  private String requestBody;
  /** 请求参数 */
  private String requestParams;
  /** 用户id */
  private Long userId;
  /** 登录ip */
  private String userIp;
  /** 登录标识 */
  private String userAgent;
  /** 开始请求时间 */
  private LocalDateTime beginTime;
  /** 结束请求时间 */
  private LocalDateTime endTime;
  /** 执行时长 */
  private Long duration;
  /** 响应状态码 */
  private Integer responseCode;
  /** 响应消息 */
  private String responseMessage;
  /** 逻辑删除 */
  @TableLogic
  @TableField("is_deleted")
  @BindDict("logic_delete")
  private Boolean deleted;
}
