package com.illidan.core.publisher;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson2.JSON;
import com.illidan.core.entity.ApiErrorLog;
import com.illidan.core.event.ApiErrorLogEvent;
import com.illidan.core.util.CoreUtil;
import com.illidan.launch.NetUtil;
import com.illidan.tool.R;
import org.apache.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * @author ben
 * @since 2022/5/23
 */
public class ApiErrorLogPublisher {

  /**
   * 发布事件
   *
   * @param t 异常
   * @param message 异常信息
   * @param r 返回体
   */
  public static void publishEvent(Throwable t, String message, R<?> r) {
    HttpServletRequest request = NetUtil.getRequest();
    ApiErrorLog log =
        ApiErrorLog.builder()
            .requestId(NetUtil.getRequestId())
            // TODO: 2022/5/23 mock待改
            .userId(1L)
            .appName(SpringUtil.getApplicationName())
            .requestMethod(request.getMethod())
            .requestUrl(request.getRequestURI())
            .requestParams(JSON.toJSONString(request.getParameterMap()))
            .requestBody(CoreUtil.getRequestBody(request))
            .userIp(NetUtil.getRequestIp())
            .userAgent(request.getHeader(HttpHeaders.USER_AGENT))
            .exceptionTime(LocalDateTime.now())
            .exceptionMessage(message)
            .responseCode(r.getCode())
            .processStatus(0)
            .build();
    if (t != null && ArrayUtil.isNotEmpty(t.getStackTrace())) {
      StackTraceElement[] stackTrace = t.getStackTrace();
      StackTraceElement traceElement = stackTrace[0];
      log.setExceptionMessage(t.getMessage());
      log.setExceptionRootCauseMessage(ExceptionUtil.getRootCauseMessage(t));
      log.setExceptionStackTrace(ArrayUtil.toString(stackTrace));
      log.setExceptionLineNumber(traceElement.getLineNumber());
      log.setExceptionClassName(traceElement.getClassName());
      log.setExceptionFileName(traceElement.getFileName());
      log.setExceptionMethodName(traceElement.getMethodName());
    }
    SpringUtil.publishEvent(ApiErrorLogEvent.of(log));
  }
}
