package com.illidan.core.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.nacos.common.utils.HttpMethod;
import com.illidan.core.config.RequestBodyWrapper;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author ben
 * @since 2022/6/7
 */
public class CoreUtil {

  /**
   * 获取请求体，只获取post请求方式的请求体
   *
   * @param request 请求
   * @return {@link String}
   */
  public static String getRequestBody(HttpServletRequest request) {
    if (null == request) {
      return StrUtil.EMPTY;
    }
    try {
      RequestBodyWrapper bodyWrapper = new RequestBodyWrapper(request);
      return bodyWrapper.getBody();
    } catch (IOException e) {
      return StrUtil.EMPTY;
    }
  }
}
