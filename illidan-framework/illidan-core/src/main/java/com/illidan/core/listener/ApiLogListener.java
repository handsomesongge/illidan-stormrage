package com.illidan.core.listener;

import com.illidan.core.feign.IApiLogFeign;
import com.illidan.core.event.ApiLogEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

/**
 * @author ben
 * @since 2022/5/27
 */
@Slf4j
@RequiredArgsConstructor
public class ApiLogListener {

  private final IApiLogFeign apiLogFeign;

  @Async
  @EventListener(ApiLogEvent.class)
  public void apiLog(ApiLogEvent event) {
    apiLogFeign.saveAccessLog(event.getLog());
  }
}
