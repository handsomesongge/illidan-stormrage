package com.illidan.core.event;

import com.illidan.core.entity.ApiErrorLog;
import org.springframework.context.ApplicationEvent;

/**
 * @author ben
 * @since 2022/5/23
 */
public class ApiErrorLogEvent extends ApplicationEvent {

  private static final long serialVersionUID = 2323957336568263112L;

  public ApiErrorLogEvent(ApiErrorLog source) {
    super(source);
  }

  public static ApiErrorLogEvent of(ApiErrorLog log) {
    return new ApiErrorLogEvent(log);
  }

  /**
   * 获取日志
   *
   * @return {@link ApiErrorLog}
   */
  public ApiErrorLog getLog() {
    return (ApiErrorLog) this.getSource();
  }
}
