package com.illidan.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.illidan.db.annotation.BindDict;
import com.illidan.db.model.PrimaryKeyEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

/**
 * @author ben
 * @since 2022/5/20
 */
@Data
@SuperBuilder
@NoArgsConstructor
@TableName("illidan_api_error_log")
public class ApiErrorLog extends PrimaryKeyEntity {

  /** 应用名 */
  private String appName;
  /** 请求id */
  private String requestId;
  /** 请求方法名 */
  private String requestMethod;
  /** 请求地址 */
  private String requestUrl;
  /** 请求体 */
  private String requestBody;
  /** 请求参数 */
  private String requestParams;
  /** 用户id */
  private Long userId;
  /** 登录ip */
  private String userIp;
  /** 登录标识 */
  private String userAgent;
  /** 异常发生时间 */
  private LocalDateTime exceptionTime;
  /** 异常名 */
  private String exceptionName;
  /** 异常导致的消息 */
  private String exceptionMessage;
  /** 异常导致的根消息 */
  private String exceptionRootCauseMessage;
  /** 异常的栈轨迹 */
  private String exceptionStackTrace;
  /** 异常发生的类全名 */
  private String exceptionClassName;
  /** 异常发生的类文件 */
  private String exceptionFileName;
  /** 异常发生的方法名 */
  private String exceptionMethodName;
  /** 异常发生的方法所在行 */
  private Integer exceptionLineNumber;
  /** 响应状态码 */
  private Integer responseCode;
  /** 处理状态 */
  @BindDict("processStatus")
  private Integer processStatus;
  /** 处理时间 */
  private LocalDateTime processTime;
  /** 处理备注 */
  private String processRemark;
  /** 逻辑删除 */
  @TableLogic
  @TableField("is_deleted")
  @BindDict("logic_delete")
  private Boolean deleted;
}
