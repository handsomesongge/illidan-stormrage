package com.illidan.core.publisher;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson2.JSON;
import com.illidan.core.entity.ApiAccessLog;
import com.illidan.core.event.ApiLogEvent;
import com.illidan.core.util.CoreUtil;
import com.illidan.launch.NetUtil;
import com.illidan.tool.R;
import org.apache.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * @author ben
 * @since 2022/5/23
 */
public class ApiLogPublisher {

  /**
   * 发布日志事件
   *
   * @param beginTime 开始时间
   * @param endTime 结束时间
   * @param duration 持续时间
   * @param results 结果
   */
  public static void publishEvent(
      LocalDateTime beginTime, LocalDateTime endTime, Long duration, R<?> results) {
    HttpServletRequest request = NetUtil.getRequest();
    ApiAccessLog log =
        ApiAccessLog.builder()
            .requestId(NetUtil.getRequestId())
            // TODO: 2022/5/23 mock待改
            .userId(1L)
            .appName(SpringUtil.getApplicationName())
            .requestMethod(request.getMethod())
            .requestUrl(request.getRequestURI())
            .requestParams(JSON.toJSONString(request.getParameterMap()))
            .requestBody(CoreUtil.getRequestBody(request))
            .userIp(NetUtil.getRequestIp())
            .userAgent(request.getHeader(HttpHeaders.USER_AGENT))
            .beginTime(beginTime)
            .endTime(endTime)
            .duration(duration)
            .responseCode(results.getCode())
            .responseMessage(results.getMessage())
            .build();
    SpringUtil.publishEvent(ApiLogEvent.of(log));
  }
}
