package com.illidan.core.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.illidan.launch.NetUtil;
import lombok.SneakyThrows;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

/**
 * @author ben
 * @since 2022/6/28
 */
public class JwtUtil {

  @SneakyThrows
  public static JSONObject getJwtPayload() {
    JSONObject jsonObject = null;
    String payload = NetUtil.getRequest().getHeader("payload");
    if (StrUtil.isNotBlank(payload)) {
      jsonObject = JSONUtil.parseObj(URLDecoder.decode(payload, StandardCharsets.UTF_8.name()));
    }

    return jsonObject;
  }
}
