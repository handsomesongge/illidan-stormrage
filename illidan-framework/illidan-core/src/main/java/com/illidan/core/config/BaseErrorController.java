package com.illidan.core.config;

import com.alibaba.fastjson2.support.spring.webservlet.view.FastJsonJsonView;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author ben
 * @since 2022/6/9
 */
public class BaseErrorController extends BasicErrorController {
  public BaseErrorController(ErrorAttributes errorAttributes, ErrorProperties errorProperties) {
    super(errorAttributes, errorProperties);
  }

  @Override
  public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
    boolean includeStackTrace = isIncludeStackTrace(request, MediaType.ALL);
    Map<String, Object> body =
        getErrorAttributes(
            request,
            (includeStackTrace)
                ? ErrorAttributeOptions.of(ErrorAttributeOptions.Include.STACK_TRACE)
                : ErrorAttributeOptions.defaults());
    HttpStatus status = getStatus(request);
    response.setStatus(status.value());
    FastJsonJsonView view = new FastJsonJsonView();
    return new ModelAndView(view, body);
  }
}
