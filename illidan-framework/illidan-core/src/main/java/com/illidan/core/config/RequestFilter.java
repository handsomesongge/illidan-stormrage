package com.illidan.core.config;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author ben
 * @since 2022/6/15
 */
@Component
public class RequestFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    Filter.super.init(filterConfig);
  }

  @Override
  public void doFilter(
      ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
      throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) servletRequest;
    RequestBodyWrapper requestWrapper = new RequestBodyWrapper(request);
    filterChain.doFilter(requestWrapper, servletResponse);
  }

  @Override
  public void destroy() {
    Filter.super.destroy();
  }
}
