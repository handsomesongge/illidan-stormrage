package com.illidan.core.feign;

import com.illidan.core.entity.ApiAccessLog;
import com.illidan.core.entity.ApiErrorLog;
import com.illidan.launch.BaseAppName;
import com.illidan.tool.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author ben
 * @since 2022/5/20
 */
@FeignClient(value = BaseAppName.ILLIDAN_SYSTEM, fallback = ApiLogFeignFallback.class)
public interface IApiLogFeign {
  String BASE_API = "/log";

  /**
   * 保存访问日志
   *
   * @param apiAccessLog api访问日志
   * @return {@link R}<{@link Void}>
   */
  @PostMapping(BASE_API + "/save/accessLog")
  R<String> saveAccessLog(@RequestBody ApiAccessLog apiAccessLog);

  /**
   * 保存错误日志
   *
   * @param apiErrorLog api错误日志
   * @return {@link R}<{@link Void}>
   */
  @PostMapping(BASE_API + "/save/errorLog")
  R<String> saveErrorLog(@RequestBody ApiErrorLog apiErrorLog);
}
