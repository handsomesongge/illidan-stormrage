package com.illidan.core.config;

import com.alibaba.fastjson2.support.config.FastJsonConfig;
import com.alibaba.fastjson2.support.spring.http.converter.FastJsonHttpMessageConverter;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.List;

import static com.alibaba.fastjson2.JSONReader.Feature.TrimString;
import static com.alibaba.fastjson2.JSONWriter.Feature.*;

/**
 * @author ben
 * @since 2022/6/4
 */
@Configuration
public class WebMvcConfig {

  /**
   * 使用fastjson代替jackson json
   *
   * @return {@link HttpMessageConverters}
   */
  @Bean
  public HttpMessageConverters fastjsonHttpMessageConverter() {
    // 1.定义一个converters转换消息的对象
    FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
    // 2.添加fastjson的配置信息，比如: 是否需要格式化返回的json数据
    FastJsonConfig fastJsonConfig = new FastJsonConfig();
    // 3.配置选用
    fastJsonConfig.setWriterFeatures(
        // 输出格式整理
        PrettyFormat,
        // 在大范围超过JavaScript支持的整数，输出为字符串格式
        BrowserCompatible,
        // 序列化BigDecimal使用toPlainString，避免科学计数法
        WriteBigDecimalAsPlain,
        // 将null list转为空数组
        WriteNullListAsEmpty,
        // 将null string转为空串
        WriteNullStringAsEmpty);
    fastJsonConfig.setReaderFeatures(
        // 修剪字符串
        TrimString);
    List<MediaType> supportedMediaTypes = new ArrayList<>();
    supportedMediaTypes.add(MediaType.APPLICATION_JSON);
    supportedMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
    supportedMediaTypes.add(MediaType.APPLICATION_ATOM_XML);
    supportedMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
    supportedMediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
    supportedMediaTypes.add(MediaType.APPLICATION_PDF);
    supportedMediaTypes.add(MediaType.APPLICATION_RSS_XML);
    supportedMediaTypes.add(MediaType.APPLICATION_XHTML_XML);
    supportedMediaTypes.add(MediaType.APPLICATION_XML);
    supportedMediaTypes.add(MediaType.IMAGE_GIF);
    supportedMediaTypes.add(MediaType.IMAGE_JPEG);
    supportedMediaTypes.add(MediaType.IMAGE_PNG);
    supportedMediaTypes.add(MediaType.TEXT_EVENT_STREAM);
    supportedMediaTypes.add(MediaType.TEXT_HTML);
    supportedMediaTypes.add(MediaType.TEXT_MARKDOWN);
    supportedMediaTypes.add(MediaType.TEXT_PLAIN);
    supportedMediaTypes.add(MediaType.TEXT_XML);
    // 4.在converter中添加配置信息
    fastConverter.setSupportedMediaTypes(supportedMediaTypes);
    fastConverter.setFastJsonConfig(fastJsonConfig);
    // 5.将converter赋值给HttpMessageConverter
    return new HttpMessageConverters(fastConverter);
  }
}
