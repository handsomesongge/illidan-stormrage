package com.illidan.core.feign;

import com.illidan.core.entity.ApiAccessLog;
import com.illidan.core.entity.ApiErrorLog;
import com.illidan.tool.R;
import com.illidan.tool.ResultCodeEnum;
import org.springframework.stereotype.Component;

/**
 * @author ben
 * @since 2022/5/20
 */
@Component
public class ApiLogFeignFallback implements IApiLogFeign {

	@Override
	public R<String> saveAccessLog(ApiAccessLog apiAccessLog) {
		return R.code(ResultCodeEnum.R_SERVER_FAILED);
	}

	@Override
	public R<String> saveErrorLog(ApiErrorLog apiErrorLog) {
		return R.code(ResultCodeEnum.R_SERVER_FAILED);
	}

}
