package com.illidan.core.aspect;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.nacos.common.utils.HttpMethod;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.illidan.launch.NetUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Map;
import java.util.Objects;

/**
 * @author ben
 * @since 2022/6/10
 */
@Order(1)
@Aspect
@Slf4j
public class RequestLogAspect {

  /**
   * 输出请求日志
   *
   * <pre>
   * -------------------- request begin --------------------
   * request-id 83bb00ac-0f1a-4b46-94b0-c050fcc625be
   * ==> GET http://localhsot:8080/
   * [header]
   * content-type = application/json
   * ...
   * [params]
   * key = value
   * [body] // 如果是post请求
   * -------------------- request end --------------------
   * -------------------- response begin --------------------
   * request-id 83bb00ac-0f1a-4b46-94b0-c050fcc625be
   * [result]
   * R json
   * <== GET http://localhsost:8080/ (100 ms)
   * -------------------- response end --------------------
   * </pre>
   *
   * @param point 切点
   * @return {@link Object}
   * @throws Throwable throwable
   */
  @Around(
      "execution(!static com.illidan.tool.R *(..)) && @within(org.springframework.web.bind.annotation.RestController)")
  public Object aroundRequestLog(ProceedingJoinPoint point) throws Throwable {
    StringBuilder requestBuilder = StrUtil.builder(200);
    HttpServletRequest request = Objects.requireNonNull(NetUtil.getRequest());
    requestBuilder.append("\n-------------------- request begin --------------------\n");
    String requestId = NetUtil.getRequestId(request);
    if (StrUtil.isNotBlank(requestId)) {
      requestBuilder.append(NetUtil.LOG_REQUEST_ID).append(StrUtil.SPACE).append(requestId);
    }
    String requestMethod = request.getMethod();
    String uri = request.getRequestURI();
    requestBuilder
        .append("==> ")
        .append(requestMethod)
        .append(StrUtil.TAB)
        .append(uri)
        .append(StrUtil.LF)
        .append("[header]")
        .append(StrUtil.LF);
    Enumeration<String> headerNames = request.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headName = headerNames.nextElement();
      String headValue = request.getHeader(headName);
      requestBuilder
          .append(headName)
          .append(StrUtil.SPACE)
          .append(StringPool.EQUALS)
          .append(StrUtil.SPACE)
          .append(headValue)
          .append(StrUtil.LF);
    }
    Map<String, String[]> parameterMap = request.getParameterMap();
    if (CollUtil.isNotEmpty(parameterMap)) {
      requestBuilder.append("[params]").append(StrUtil.LF);
      parameterMap.forEach(
          (k, v) ->
              requestBuilder
                  .append(k)
                  .append(StrUtil.SPACE)
                  .append(StringPool.EQUALS)
                  .append(StrUtil.SPACE)
                  .append(ArrayUtil.toString(v))
                  .append(StrUtil.LF));
    }
    if (HttpMethod.POST.equals(requestMethod)) {
      requestBuilder
          .append("[body]")
          .append(StrUtil.LF)
          .append(ArrayUtil.toString(point.getArgs()));
    }
    requestBuilder.append("\n-------------------- request end --------------------\n");
    log.info(requestBuilder.toString());
    StringBuilder responseBuilder = StrUtil.builder(200);
    long beginTime = System.currentTimeMillis();
    responseBuilder.append("\n-------------------- response begin --------------------\n");
    if (StrUtil.isNotBlank(requestId)) {
      requestBuilder.append(NetUtil.LOG_REQUEST_ID).append(StrUtil.SPACE).append(requestId);
    }
    try {
      Object result = point.proceed();
      responseBuilder
          .append("[result]")
          .append(StrUtil.LF)
          .append(JSON.toJSONString(result, JSONWriter.Feature.PrettyFormat));
      return result;
    } finally {
      long duration = System.currentTimeMillis() - beginTime;
      responseBuilder
          .append("\n<== ")
          .append(requestMethod)
          .append(StrUtil.SPACE)
          .append(StringPool.EQUALS)
          .append(StrUtil.SPACE)
          .append(uri)
          .append(StrUtil.SPACE)
          .append(StringPool.LEFT_BRACKET)
          .append(duration)
          .append(" ms)");
      responseBuilder.append("\n-------------------- response end --------------------\n");
      log.info(responseBuilder.toString());
    }
  }
}
