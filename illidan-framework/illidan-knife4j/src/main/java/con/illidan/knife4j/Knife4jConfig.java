package con.illidan.knife4j;

import com.illidan.launch.AppContext;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ParameterType;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.service.Response;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Knife4j 配置
 *
 * @author ben
 * @since 2022/5/17
 */
@EnableSwagger2
@RequiredArgsConstructor
@Configuration(proxyBeanMethods = false)
public class Knife4jConfig {

  private final AppContext appContext;

  @Bean
  public Docket docket() {
    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo())
        .groupName(Docket.DEFAULT_GROUP_NAME)
        .select()
        .apis(
            RequestHandlerSelectors.basePackage(
                "com.illidan." + appContext.getShortAppName() + ".controller"))
        .paths(PathSelectors.any())
        .build();
  }

  /**
   * api信息
   *
   * @return {@link ApiInfo}
   */
  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title(appContext.getAppName() + "-Swagger接口文档")
        .version("1.0.0")
        .build();
  }

  /** 封装全局通用参数 */
  private List<RequestParameter> getGlobalRequestParameters() {
    List<RequestParameter> parameters = new ArrayList<>();
    parameters.add(
        new RequestParameterBuilder()
            .name("Authorization")
            .description("令牌")
            .required(false)
            .in(ParameterType.HEADER)
            .query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
            .build());
    return parameters;
  }

  /** 封装通用响应信息 */
  private List<Response> getGlobalResponseMessage() {
    List<Response> responseList = new ArrayList<>();
    responseList.add(new ResponseBuilder().code("404").description("未找到资源").build());
    return responseList;
  }
}
