package com.illidan.db.model;

import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.fastjson2.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author ben
 * @since 2022/5/17
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class ExtendEntity extends PrimaryKeyEntity {

  @TableLogic
  @JSONField(serializeFeatures = JSONWriter.Feature.WriteNonStringValueAsString)
  @TableField("is_deleted")
  private Boolean deleted;
}
