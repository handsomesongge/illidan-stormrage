package com.illidan.db.feign;

import com.illidan.cloud.constant.CloudConstant;
import com.illidan.launch.BaseAppName;
import com.illidan.tool.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author ben
 * @since 2022/5/18
 */
@FeignClient(value = BaseAppName.ILLIDAN_DICT, fallback = DictInterceptorFeignFallback.class)
public interface IDictInterceptorFeign {

  String BASE_API = CloudConstant.FEIGN_BASE_API + "/dict";
  String GET_NAME = BASE_API + "/getName";

  /**
   * 根据字典码和字典值获取字典名称
   *
   * @param code 字典码
   * @param number 字典值
   * @return {@link R}<{@link String}>
   */
  @Validated
  @GetMapping(GET_NAME)
  R<String> getName(
      @RequestParam @NotBlank(message = "字典码不能为空") String code,
      @RequestParam @NotNull(message = "字典值不能为空") Integer number);
}
