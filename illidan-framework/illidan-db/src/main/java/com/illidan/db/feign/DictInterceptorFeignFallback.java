package com.illidan.db.feign;

import com.illidan.tool.R;
import org.springframework.stereotype.Component;

/**
 * @author ben
 * @since 2022/6/15
 */
@Component
public class DictInterceptorFeignFallback implements IDictInterceptorFeign {
    @Override
    public R<String> getName(String code, Integer number) {
        return R.fail("调用字典接口失败");
    }
}
