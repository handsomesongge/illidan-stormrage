package com.illidan.db.model;

import org.springframework.beans.factory.annotation.Autowired;

/**基本控制器
 * @author ben
 * @since 2022/6/9
 @date 2022/06/20
 */
public class BaseController<S extends IBaseService<?>> extends AbstractController {

  protected S baseService;

  @Autowired
  public void setBaseService(S baseService) {
    this.baseService = baseService;
  }
}
