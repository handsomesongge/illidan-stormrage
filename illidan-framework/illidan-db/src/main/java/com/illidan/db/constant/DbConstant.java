package com.illidan.db.constant;

/**
 * @author ben
 * @since 2022/5/17
 */
public interface DbConstant {

  String QUERY_EQUAL = "e";
  String QUERY_NOT_EQUAL = "ne";
  String QUERY_LIKE = "l";
  String QUERY_LIKE_LEFT = "ll";
  String QUERY_LIKE_RIGHT = "lr";
  String QUERY_NOT_LIKE = "nl";
  String QUERY_GT = "gt";
  String QUERY_LT = "lt";
  String QUERY_GE = "ge";
  String QUERY_LE = "le";
  String QUERY_IS_NULL = "in";
  String QUERY_NOT_NULL = "nn";
  String ORDER_ASC = "asc";
  String ORDER_DESC = "desc";
}
