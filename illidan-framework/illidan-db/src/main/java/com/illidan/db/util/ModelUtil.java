package com.illidan.db.util;

import com.illidan.db.model.BaseEntity;
import com.illidan.db.model.PrimaryKeyEntity;

import java.time.LocalDateTime;

/**
 * @author ben
 * @since 2022/5/18
 */
public class ModelUtil {

  public static void resolveModel(PrimaryKeyEntity model) {
    if (model != null && model instanceof BaseEntity) {
      // 用是否有id判断是insert还是update
      if (model.getId() != null) {
        ((BaseEntity) model).setCreateTime(LocalDateTime.now());
      } else {
        ((BaseEntity) model).setUpdateTime(LocalDateTime.now());
      }
    }
  }
}
