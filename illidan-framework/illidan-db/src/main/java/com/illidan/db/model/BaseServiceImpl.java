package com.illidan.db.model;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.enums.SqlMethod;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.illidan.db.util.ModelUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.Collection;
import java.util.List;

/**
 * @author ben
 * @since 2022/5/18
 */
public class BaseServiceImpl<M extends BaseMapper<E>, E extends PrimaryKeyEntity>
    extends ServiceImpl<M, E> implements IBaseService<E> {

  /**
   * bug: ClassCastException ServiceImpl#currentMapperClass 和 currentModelClass 实现方式问题导致源码的强转使用的是
   * {@link ServiceImpl} 的泛型<{@link M},{@link E}>
   */
  @Override
  @SuppressWarnings("unchecked")
  protected Class<M> currentMapperClass() {
    return (Class<M>)
        ReflectionKit.getSuperClassGenericType(this.getClass(), BaseServiceImpl.class, 0);
  }

  /**
   * 当前模型类
   *
   * @return {@link Class}<{@link E}>
   */
  @Override
  @SuppressWarnings("unchecked")
  protected Class<E> currentModelClass() {
    return (Class<E>)
        ReflectionKit.getSuperClassGenericType(this.getClass(), BaseServiceImpl.class, 1);
  }

  @Override
  public boolean deleteLogicByIds(List<Long> list) {
    String sqlStatement = getSqlStatement(SqlMethod.LOGIC_DELETE_BATCH_BY_IDS);
    TableInfo tableInfo = TableInfoHelper.getTableInfo(entityClass);
    return executeBatch(
        list,
        (sqlSession, e) -> {
          if (entityClass.isAssignableFrom(e.getClass())) {
            sqlSession.update(sqlStatement, e);
          } else {
            E instance = tableInfo.newInstance();
            tableInfo.setPropertyValue(instance, tableInfo.getKeyProperty(), e);
            sqlSession.update(sqlStatement, instance);
          }
        });
  }

  @Override
  public boolean deleteLogicById(Long id) {
    String sqlStatement = getSqlStatement(SqlMethod.LOGIC_DELETE_BY_ID);
    TableInfo tableInfo = TableInfoHelper.getTableInfo(entityClass);
    try (SqlSession sqlSession = SqlHelper.sqlSession(entityClass)) {
      if (this.entityClass.isAssignableFrom(entityClass)) {
        return SqlHelper.retBool(sqlSession.update(sqlStatement, entityClass));
      } else {
        E instance = tableInfo.newInstance();
        tableInfo.setPropertyValue(instance, tableInfo.getKeyProperty(), entityClass);
        return SqlHelper.retBool(sqlSession.update(sqlStatement, instance));
      }
    }
  }

  @Override
  public boolean saveOrUpdate(E entity) {
    ModelUtil.resolveModel(entity);
    return super.saveOrUpdate(entity);
  }

  @Override
  public boolean save(E entity) {
    ModelUtil.resolveModel(entity);
    return super.save(entity);
  }

  @Override
  public boolean saveBatch(Collection<E> entityList) {
    entityList.forEach(ModelUtil::resolveModel);
    return super.saveBatch(entityList);
  }

  @Override
  public boolean saveOrUpdateBatch(Collection<E> entityList) {
    entityList.forEach(ModelUtil::resolveModel);
    return super.saveOrUpdateBatch(entityList);
  }

  @Override
  public boolean updateById(E entity) {
    ModelUtil.resolveModel(entity);
    return super.updateById(entity);
  }

  @Override
  public boolean update(E entity, Wrapper<E> updateWrapper) {
    ModelUtil.resolveModel(entity);
    return super.update(entity, updateWrapper);
  }

  @Override
  public boolean updateBatchById(Collection<E> entityList) {
    entityList.forEach(ModelUtil::resolveModel);
    return super.updateBatchById(entityList);
  }

  @Override
  public Page<E> page(PageQuery query) {
    IPage<E> page = getBaseMapper().selectPage(query.buildPage(), query.buildWrapper());
    return Page.<E>builder()
        .current(page.getCurrent())
        .size(page.getSize())
        .total(page.getTotal())
        .records(page.getRecords())
        .build();
  }
}
