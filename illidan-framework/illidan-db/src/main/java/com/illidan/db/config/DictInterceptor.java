package com.illidan.db.config;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.stream.StreamUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.illidan.db.annotation.BindDict;
import com.illidan.db.feign.IDictInterceptorFeign;
import com.illidan.tool.$;
import com.illidan.tool.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.sql.Statement;
import java.util.Collection;
import java.util.Optional;

/**
 * @author ben
 * @since 2022/5/18
 */
@Slf4j
@Component
@Intercepts({
  @Signature(
      type = ResultSetHandler.class,
      method = "handleResultSets",
      args = {Statement.class})
})
@RequiredArgsConstructor
public class DictInterceptor implements Interceptor {
  private final IDictInterceptorFeign dictFeign;

  @Override
  public Object intercept(Invocation invocation) throws Throwable {
    // 获取执行结果
    Object result = invocation.proceed();
    // 执行结果默认是singleList类型
    if (result instanceof Collection<?>) {
      Optional<Boolean> existedAnnotation =
          StreamUtil.of(ReflectUtil.getFields(ClassUtil.getTypeArgument(result.getClass())))
              .map(field -> AnnotationUtil.hasAnnotation(field, BindDict.class))
              .filter(Boolean::booleanValue)
              .findAny();
      if (existedAnnotation.isPresent()) {
        ((Collection<?>) result).forEach(this::dictFieldFilling);
      }
    }
    return result;
  }

  /**
   * 字典字段填充
   *
   * @param object object
   */
  private void dictFieldFilling(Object object) {
    // 拦截所有非基本类型或包装类型结果集
    if (!ClassUtil.isBasicType(object.getClass())) {
      // 遍历字段
      for (Field field : ReflectUtil.getFields(object.getClass())) {
        // 字段非空且使用 BindDict 注解
        if (ReflectUtil.getFieldValue(object, field) != null
            && AnnotationUtil.hasAnnotation(field, BindDict.class)) {
          // 由于使用了Spring的@AliasFor注解只能使用Spring的注解工具获取注解对象
          BindDict bindDict = AnnotationUtils.getAnnotation(field, BindDict.class);
          // 填充字段
          String targetField =
              StrUtil.blankToDefault(bindDict.targetField(), field.getName() + "Text");
          // 判断是否存在需要填充的字段
          if (ReflectUtil.hasField(object.getClass(), targetField)) {
            R<String> res =
                dictFeign.getName(
                    bindDict.code(), $.toInt(ReflectUtil.getFieldValue(object, field)));
            // 获取成功就填充字段
            if (res.isSuccess()) {
              ReflectUtil.setFieldValue(object, targetField, res.getData());
            }
          }
        }
      }
    }
  }
}
