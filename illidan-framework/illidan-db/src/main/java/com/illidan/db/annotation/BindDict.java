package com.illidan.db.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.core.annotation.AliasFor;

/**
 * @author ben
 * @since 2022/5/18
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BindDict {
  @AliasFor("code")
  String value() default "";

  /**
   * 字典码
   *
   * @return {@link String}
   */
  @AliasFor("value")
  String code() default "";

  /**
   * 被填充字段名
   *
   * @return {@link String}
   */
  String targetField() default "";
}
