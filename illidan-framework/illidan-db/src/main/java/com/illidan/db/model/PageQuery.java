package com.illidan.db.model;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.PageDTO;
import com.illidan.db.constant.DbConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import java.util.Map;

/**
 * @author ben
 * @since 2022/5/17
 */
@Data
@NoArgsConstructor
@ApiModel("查询组合")
public class PageQuery {

  @ApiModelProperty("查询条件,字段名与运算符用下划线分隔")
  private Map<String, String> conditions;

  @ApiModelProperty("查询排序,字段名与运算符用下划线分隔")
  private Map<String, String> orders;

  @ApiModelProperty("分页大小")
  @Min(value = 1, message = "分页最小为1")
  private Long size;

  @ApiModelProperty("当前页")
  @Min(value = 1, message = "页码最小为1")
  private Long current;

  /**
   * 防SQL注入
   *
   * @param param 参数
   * @return {@link String}
   */
  public static String filter(String param) {
    return param == null
        ? null
        : param.replaceAll(
            "(?i)'|%|--|insert|delete|select|count|group|union|drop|truncate|alter|grant|execute|exec|xp_cmdshell|call|declare|sql",
            "");
  }

  /**
   * 构建{@link Page}<{@link E}>
   *
   I @return {@link Page}<{@link E}>
   */
  public <E> IPage<E> buildPage() {
    return PageDTO.of(current, size);
  }

  /**
   * 构建{@link QueryWrapper}<{@link E}>
   *
   * @return {@link QueryWrapper}<{@link E}>
   */
  public <E> Wrapper<E> buildWrapper() {
    QueryWrapper<E> query = new QueryWrapper<>();
    if (CollUtil.isNotEmpty(conditions)) {
      spliceCondition(query);
    }
    if (CollUtil.isNotEmpty(orders)) {
      spliceOrder(query);
    }
    return query;
  }

  /**
   * 拼接排序
   *
   * @param query 查询
   */
  private <E> void spliceOrder(QueryWrapper<E> query) {
    orders.forEach(
        (key, value) -> {
          if (StrUtil.isNotBlank(key)) {
            String column = StrUtil.toUnderlineCase(key);
            if (StrUtil.equals(DbConstant.ORDER_DESC, value)) {
              query.orderByDesc(column);
            } else {
              query.orderByAsc(column);
            }
          }
        });
  }

  /**
   * 拼接条件
   *
   * @param query 查询
   */
  private <E> void spliceCondition(QueryWrapper<E> query) {
    conditions
        .keySet()
        .forEach(
            key -> {
              String value = filter(conditions.get(key));
              conditions.put(key, value);
            });
    conditions.forEach(
        (key, value) -> {
          if (StrUtil.isNotBlank(key)
              && StrUtil.contains(key, StrPool.UNDERLINE)
              && StrUtil.isNotBlank(value)) {
            String column = StrUtil.toUnderlineCase(ReUtil.getGroup0("(^[a-zA-Z]+)", key));
            String v = filter(value);
            switch (ReUtil.getGroup0("(?<=_)([a-zA-Z]+)", key)) {
              case DbConstant.QUERY_EQUAL:
                query.eq(column, v);
                break;
              case DbConstant.QUERY_NOT_EQUAL:
                query.ne(column, v);
                break;
              case DbConstant.QUERY_LIKE:
                query.like(column, v);
                break;
              case DbConstant.QUERY_LIKE_LEFT:
                query.likeLeft(column, v);
                break;
              case DbConstant.QUERY_LIKE_RIGHT:
                query.likeRight(column, v);
                break;
              case DbConstant.QUERY_NOT_LIKE:
                query.notLike(column, v);
                break;
              case DbConstant.QUERY_GT:
                query.gt(column, v);
                break;
              case DbConstant.QUERY_LT:
                query.lt(column, v);
                break;
              case DbConstant.QUERY_GE:
                query.ge(column, v);
                break;
              case DbConstant.QUERY_LE:
                query.le(column, v);
                break;
              case DbConstant.QUERY_IS_NULL:
                query.isNull(column);
                break;
              case DbConstant.QUERY_NOT_NULL:
                query.isNotNull(column);
                break;
              default:
            }
          }
        });
  }
}
