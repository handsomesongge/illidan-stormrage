package com.illidan.db.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.IllegalSQLInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.illidan.launch.AppContext;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ben
 * @since 2022/5/18
 */
@Configuration
@RequiredArgsConstructor
public class MybatisPlusConfig {

  private final AppContext appContext;

  /**
   * @return {@link MybatisPlusInterceptor}
   */
  @Bean
  public MybatisPlusInterceptor mybatisPlusInterceptor() {
    MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
    // 乐观锁
    interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
    // 分页插件
    interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
    // 字典插件
    if (appContext.isDev()) {
      // 防全表更新与删除插件
      interceptor.addInnerInterceptor(new BlockAttackInnerInterceptor());
      // sql性能规范
      interceptor.addInnerInterceptor(new IllegalSQLInnerInterceptor());
    }
    return interceptor;
  }
}
