package com.illidan.db.model;

import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ben
 * @since 2022/5/18
 */
public interface IBaseService<E extends PrimaryKeyEntity> extends IService<E> {

  /**
   * 删除逻辑由ids
   *
   * @param ids ids
   * @return boolean
   */
  boolean deleteLogicByIds(@NotEmpty List<Long> ids);

  /**
   * 删除逻辑通过id
   *
   * @param id id
   * @return boolean
   */
  boolean deleteLogicById(@NotNull Long id);

  /**
   * 分页查询
   *
   * @param query 查询
   * @return {@link Page}<{@link E}>
   */
  Page<E> page(PageQuery query);
}
