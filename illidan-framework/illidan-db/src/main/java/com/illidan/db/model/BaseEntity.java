package com.illidan.db.model;

import cn.hutool.core.date.DatePattern;
import com.alibaba.fastjson2.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.Version;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

/**
 * @author ben
 * @since 2022/5/17
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class BaseEntity extends ExtendEntity {

  @ApiModelProperty("创建人")
  private Long createId;

  @ApiModelProperty("更新人")
  private Long updateId;

  @ApiModelProperty("创建时间")
  @JSONField(format = DatePattern.NORM_DATETIME_PATTERN)
  private LocalDateTime createTime;

  @ApiModelProperty("更新时间")
  @JSONField(format = DatePattern.NORM_DATETIME_PATTERN)
  private LocalDateTime updateTime;

  @Version
  @ApiModelProperty(hidden = true)
  private Integer version;
}
