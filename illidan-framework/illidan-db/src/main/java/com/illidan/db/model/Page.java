package com.illidan.db.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

/**
 * @author ben
 * @since 2022/5/17
 */
@Data
@Builder
@Accessors(chain = true)
@ApiModel("分页实体")
public class Page<T> {

  @ApiModelProperty("当前页码")
  private long current;

  @ApiModelProperty("页码大小")
  private long size;

  @ApiModelProperty("分页大小")
  private long total;

  @ApiModelProperty("分页数据")
  private List<T> records;

  public static <T> Page<T> of(long current, long size) {
    return Page.<T>builder().current(current).size(size).build();
  }

  @SuppressWarnings("unchecked")
  public <R> Page<R> convert(Function<? super T, ? extends R> mapper) {
    List<R> collect = this.getRecords().stream().map(mapper).collect(toList());
    return ((Page<R>) this).setRecords(collect);
  }
}
