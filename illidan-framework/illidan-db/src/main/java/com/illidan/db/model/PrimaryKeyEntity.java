package com.illidan.db.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * @author ben
 * @since 2022/5/17
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class PrimaryKeyEntity implements Serializable {

  @TableId(type = IdType.ASSIGN_ID)
  @ApiModelProperty(name = "主键")
  private Long id;
}
