package com.illidan.db.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author ben
 * @since 2022/5/18
 */
@Data
@SuperBuilder
@NoArgsConstructor
public class BaseVO {

  @ApiModelProperty("主键")
  private Long id;
}
