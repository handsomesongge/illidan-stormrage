package com.illidan.db.model;

import com.illidan.tool.R;

/**
 * @author ben
 * @since 2022/5/18
 */
public abstract class AbstractController{

  public <T> R<T> data(T data) {
    return R.data(data);
  }

  public R<String> ok() {
    return R.ok();
  }

  public R<String> ok(String message) {
    return R.ok(message);
  }

  public R<String> fail() {
    return R.fail();
  }

  public R<String> fail(String message) {
    return R.fail(message);
  }

  public R<String> status(boolean condition) {
    return R.status(condition);
  }
}
