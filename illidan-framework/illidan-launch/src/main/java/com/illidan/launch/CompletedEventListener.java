package com.illidan.launch;

import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

/**
 * @author ben
 * @since 2022/5/16
 */
@Slf4j
@Configuration
public class CompletedEventListener {

  @Order
  @EventListener({WebServerInitializedEvent.class})
  public void bootCompleted(WebServerInitializedEvent event) {
    Environment environment = event.getApplicationContext().getEnvironment();
    String appName = SpringUtil.getApplicationName();
    int port = event.getWebServer().getPort();
    String profile = StringUtils.arrayToCommaDelimitedString(environment.getActiveProfiles());
    long duration = System.currentTimeMillis() - event.getApplicationContext().getStartupDate();
    log.info("\n----[{}]({} ms)启动完成，端口：[{}],环境:[{}]----", appName, duration, port, profile);
  }
}
