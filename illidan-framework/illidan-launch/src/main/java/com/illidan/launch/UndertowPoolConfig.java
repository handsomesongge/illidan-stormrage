package com.illidan.launch;

import io.undertow.server.DefaultByteBufferPool;
import io.undertow.websockets.jsr.WebSocketDeploymentInfo;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Configuration;

/**
 * @author ben
 * @since 2022/5/16
 */
@Configuration
@ConditionalOnClass(WebSocketDeploymentInfo.class)
public class UndertowPoolConfig
    implements WebServerFactoryCustomizer<UndertowServletWebServerFactory> {

  @Override
  public void customize(UndertowServletWebServerFactory factory) {
    factory.addDeploymentInfoCustomizers(
        deploymentInfo -> {
          WebSocketDeploymentInfo webSocketDeploymentInfo = new WebSocketDeploymentInfo();
          webSocketDeploymentInfo.setBuffers(new DefaultByteBufferPool(false, 1024));
          deploymentInfo.addServletContextAttribute(
              "io.undertow.websockets.jsr.WebSocketDeploymentInfo", webSocketDeploymentInfo);
        });
  }
}
