package com.illidan.launch;

/**
 * app启动异常
 *
 * @author ben
 * @since 2022/5/16
 */
public class LaunchException extends RuntimeException {

  public LaunchException() {}

  public LaunchException(String message) {
    super(message);
  }

  public LaunchException(String message, Throwable cause) {
    super(message, cause);
  }

  public LaunchException(Throwable cause) {
    super(cause);
  }

  public LaunchException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
