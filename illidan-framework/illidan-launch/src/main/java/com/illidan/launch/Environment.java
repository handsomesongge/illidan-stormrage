package com.illidan.launch;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 环境枚举
 *
 * @author ben
 * @since 2022/5/16
 */
@Getter
@AllArgsConstructor
public enum Environment {
  /** dev环境 */
  DEV("dev"),
  /** prod环境 */
  PROD("prod");

  /** 文本 */
  private final String text;

  /**
   * 获得环境列表
   *
   * @return {@link List}<{@link String}>
   */
  public static List<String> getEnvironmentList() {
    return Stream.of(values()).map(Environment::getText).collect(Collectors.toList());
  }

  public static boolean isDev(String env) {
    return DEV.getText().equals(env);
  }

  public static Environment of(String profile) {
    return valueOf(profile.toUpperCase(Locale.ROOT));
  }
}
