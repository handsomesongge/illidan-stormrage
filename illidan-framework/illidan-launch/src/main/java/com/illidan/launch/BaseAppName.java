package com.illidan.launch;

/**
 * 基础app名称
 *
 * @author ben
 * @since 2022/5/18
 */
public interface BaseAppName {

  /** 应用名称前缀 */
  String NAME_PREFIX = "illidan-";

  /** 网关 */
  String ILLIDAN_GATEWAY = NAME_PREFIX + "gateway";

  /** 字典 */
  String ILLIDAN_DICT = NAME_PREFIX + "dict";

  /** 系统 */
  String ILLIDAN_SYSTEM = NAME_PREFIX + "system";

  /** 授权 */
  String ILLIDAN_AUTH = NAME_PREFIX + "auth";
}
