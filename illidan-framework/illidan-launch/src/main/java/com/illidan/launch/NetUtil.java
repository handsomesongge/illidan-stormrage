package com.illidan.launch;

import cn.hutool.core.lang.Assert;
import com.illidan.tool.ServiceException;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author ben
 * @since 2022/5/17
 */
public class NetUtil {

  public static final String REQUEST_ID = "requestId";
  public static final String LOG_REQUEST_ID = "request-id";

  /**
   * 获取{@link HttpServletRequest}
   *
   * @return {@link HttpServletRequest}
   */
  public static HttpServletRequest getRequest() {
    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    Assert.notNull(
        requestAttributes, () -> new ServiceException("request from RequestContextHolder is null"));
    return ((ServletRequestAttributes) requestAttributes).getRequest();
  }

  /**
   * get请求头
   *
   * @param key 关键
   * @return {@link String}
   */
  public static String getRequestHeader(String key) {
    return getRequest().getHeader(key);
  }

  /**
   * 得到{@link HttpServletResponse}
   *
   * @return {@link HttpServletResponse}
   */
  public static HttpServletResponse getResponse() {
    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    Assert.notNull(
        requestAttributes, () -> new ServiceException("request from RequestContextHolder is null"));
    return ((ServletRequestAttributes) requestAttributes).getResponse();
  }

  /**
   * 获取{@link HttpSession}
   *
   * @return {@link HttpSession}
   */
  public static HttpSession getSession() {
    return getRequest().getSession(true);
  }

  /**
   * 获取请求的IP地址
   *
   * @return {@link String}
   */
  public static String getRequestIp() {
    HttpServletRequest request = getRequest();
    String ip = request.getHeader("x-forwarded-for");
    String unknown = "unknown";
    if (ip == null || ip.length() == 0 || unknown.equalsIgnoreCase(ip)) {
      ip = request.getHeader("Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || unknown.equalsIgnoreCase(ip)) {
      ip = request.getHeader("X-Forwarded-For");
    }
    if (ip == null || ip.length() == 0 || unknown.equalsIgnoreCase(ip)) {
      ip = request.getHeader("WL-Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || unknown.equalsIgnoreCase(ip)) {
      ip = request.getHeader("X-Real-IP");
    }
    if (ip == null || ip.length() == 0 || unknown.equalsIgnoreCase(ip)) {
      ip = request.getRemoteAddr();
    }
    return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;
  }

  /**
   * 获取请求id
   *
   * @return {@link String}
   */
  public static String getRequestId() {
    return getRequestId(getRequest());
  }

  /**
   * 获取请求id
   *
   * @param request 请求
   * @return {@link String}
   */
  public static String getRequestId(HttpServletRequest request) {
    return request.getHeader(REQUEST_ID);
  }
}
