package com.illidan.launch;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.Ordered;

/**
 * 使用spi方式拓展单个服务启动环境配置
 *
 * @author ben
 * @since 2022/5/16
 */
public interface LaunchExtension extends Ordered, Comparable<LaunchExtension> {

  /**
   * 扩展
   *
   * @param builder 应用程序构建器
   * @param appName 应用程序名称
   * @param env 环境
   */
  void extension(SpringApplicationBuilder builder, String appName, String env);

  /**
   * 比较配置拓展优先级
   *
   * @param service 服务
   * @return int
   */
  @Override
  default int compareTo(LaunchExtension service) {
    return Integer.compare(this.getOrder(), service.getOrder());
  }

  /**
   * 获取优先级，默认最小
   *
   * @return int
   */
  @Override
  default int getOrder() {
    return Ordered.HIGHEST_PRECEDENCE;
  }
}
