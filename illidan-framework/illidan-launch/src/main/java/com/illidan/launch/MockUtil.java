package com.illidan.launch;

import cn.hutool.core.lang.func.VoidFunc1;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import uk.co.jemos.podam.api.PodamFactoryImpl;

/**
 * @author ben
 * @since 2022/5/16
 */
public class MockUtil {

  /**
   * 随机对象,必须包含无参构造函数,不处理父类字段
   *
   * @param clazz clazz
   * @return {@link T}
   */
  public static <T> T mockObject(Class<T> clazz) {
    PodamFactoryImpl factory = new PodamFactoryImpl();
    return factory.manufacturePojo(clazz);
  }

  /**
   * 随机对象,必须包含无参构造函数,不处理父类字段
   *
   * @param clazz clazz
   * @return {@link T}
   */
  public static <T> T mockObject(Class<T> clazz, VoidFunc1<T> voidFunc) {
    T t = mockObject(clazz);
    if (ObjectUtil.isNotNull(voidFunc)) {
      voidFunc.callWithRuntimeException(t);
    }
    return t;
  }

  /**
   * 随机对象忽略id
   *
   * @param clazz clazz
   * @return {@link T}
   */
  public static <T> T mockObjectIgnoreId(Class<T> clazz) {
    return mockObjectIgnoreId(clazz, null);
  }

  /**
   * 随机对象忽略id
   *
   * @param clazz clazz
   * @return {@link T}
   */
  public static <T> T mockObjectIgnoreId(Class<T> clazz, VoidFunc1<T> voidFunc) {
    T t = mockObject(clazz, voidFunc);
    ReflectUtil.setFieldValue(t, "id", null);
    return t;
  }
}
