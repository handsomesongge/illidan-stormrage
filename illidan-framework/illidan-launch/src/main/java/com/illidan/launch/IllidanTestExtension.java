package com.illidan.launch;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * 拓展单元测试环境配置
 *
 * @author ben
 * @since 2022/5/16
 */
@Slf4j
public class IllidanTestExtension implements BeforeAllCallback {

  /**
   * 需要提前加载的环境配置
   *
   * @param context 上下文
   * @throws Exception 异常
   */
  @Override
  public void beforeAll(ExtensionContext context) throws Exception {
    IllidanTestApplication ann =
        context.getRequiredTestClass().getAnnotation(IllidanTestApplication.class);
    String env = ann.env().getText();
    log.info("----初始化 Junit 配置环境，服务名:[{}] --- [{}]----", ann.appName(), env);
    Properties props = System.getProperties();
    props.setProperty("spring.application.name", ann.appName());
    props.setProperty("spring.profiles.active", env);
    SpringApplicationBuilder builder = new SpringApplicationBuilder(ann.clazz());
    // 允许重复定义bean，但后者会覆盖前者
    props.setProperty("spring.main.allow-bean-definition-overriding", "true");
    // 加载自定义拓展配置
    List<LaunchExtension> launcherList = new ArrayList<>();
    ServiceLoader.load(LaunchExtension.class).forEach(launcherList::add);
    launcherList.stream()
        .sorted(Comparator.comparing(LaunchExtension::getOrder))
        .collect(Collectors.toList())
        .forEach(launcherService -> launcherService.extension(builder, ann.appName(), env));
    log.info("----Junit 配置环境初始化完成，服务名:[{}] --- [{}]----", ann.appName(), env);
  }
}
