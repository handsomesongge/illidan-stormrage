package com.illidan.launch;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.*;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 自定义启动类
 *
 * <pre>
 * 属性源优先级
 * 1 命令行参数。
 * 2 Servlet初始参数
 * 3 ServletContext初始化参数
 * 4 JVM系统属性
 * 5 操作系统环境变量
 * 6 随机生成的带random.*前缀的属性
 * 7 应用程序以外的application.yml或者application.properties文件
 * 8 classpath:/config/application.yml或者classpath:/config/application.properties
 * 9 通过@PropertySource标注的属性源
 * 10 默认属性
 * </pre>
 *
 * @author ben
 * @since 2022/5/16
 */
public class BootApplication {

  /**
   * 运行
   *
   * @param appName 应用程序名称
   * @param source 启动类
   * @param args 参数
   * @return {@link ConfigurableApplicationContext}
   */
  public static ConfigurableApplicationContext run(
      String appName, Class<?> source, String... args) {
    SpringApplicationBuilder builder = createSpringApplicationBuilder(appName, source, args);
    return builder.run(args);
  }

  private static SpringApplicationBuilder createSpringApplicationBuilder(
      String appName, Class<?> source, String[] args) {
    Properties props = System.getProperties();
    props.setProperty("illidan.boot.begin-time", Long.toString(System.currentTimeMillis()));
    Assert.hasText(appName, "[appName]服务名不能为空");
    // 读取环境变量，使用spring boot的规则
    // 创建一个标准的环境
    ConfigurableEnvironment environment = new StandardEnvironment();
    // 获取配置源，用于添加、排序数据源
    MutablePropertySources propertySources = environment.getPropertySources();
    // 将命令行参数优先级设置为最高
    // 参考
    // org.springframework.boot.SpringApplication.configurePropertySources#configurePropertySources
    propertySources.addFirst(new SimpleCommandLinePropertySource(args));
    // 将系统源的配置优先级设置最后
    propertySources.addLast(
        new MapPropertySource(
            StandardEnvironment.SYSTEM_PROPERTIES_PROPERTY_SOURCE_NAME,
            environment.getSystemProperties()));
    propertySources.addLast(
        new SystemEnvironmentPropertySource(
            StandardEnvironment.SYSTEM_ENVIRONMENT_PROPERTY_SOURCE_NAME,
            environment.getSystemEnvironment()));
    // 获取配置的环境变量
    String[] activeProfiles = environment.getActiveProfiles();
    // 判断环境:dev、pro
    List<String> profiles = Arrays.asList(activeProfiles);
    // 预设的环境
    List<String> environmentList = Environment.getEnvironmentList();
    // 交集
    environmentList.retainAll(profiles);
    // 当前使用
    List<String> activeProfileList = new ArrayList<>(profiles);
    Function<Object[], String> joinFun = StringUtils::arrayToCommaDelimitedString;
    SpringApplicationBuilder builder = new SpringApplicationBuilder(source);
    String profile;
    if (activeProfileList.isEmpty()) {
      // 默认dev开发
      profile = Environment.DEV.getText();
      activeProfileList.add(profile);
      builder.profiles(profile);
    } else if (activeProfileList.size() == 1) {
      profile = activeProfileList.get(0);
    } else {
      // 同时存在dev、prod环境时
      throw new LaunchException(
          "同时存在环境变量:[" + StringUtils.arrayToCommaDelimitedString(activeProfiles) + "]");
    }
    String startJarPath = BootApplication.class.getResource("/").getPath().split("!")[0];
    String activePros = joinFun.apply(activeProfileList.toArray());
    System.out.printf("----环境变量:[%s]，jar位置:[%s]----\n", activePros, startJarPath);
    props.setProperty("spring.application.name", appName);
    props.setProperty("spring.profiles.active", profile);
    // 允许重复定义bean，但后者会覆盖前者
    props.setProperty("spring.main.allow-bean-definition-overriding", "true");
    // 加载自定义拓展配置
    List<LaunchExtension> extensions = new ArrayList<>();
    ServiceLoader.load(LaunchExtension.class).forEach(extensions::add);
    extensions.stream()
        .sorted(Comparator.comparing(LaunchExtension::getOrder))
        .collect(Collectors.toList())
        .forEach(launcherService -> launcherService.extension(builder, appName, profile));
    return builder;
  }
}
