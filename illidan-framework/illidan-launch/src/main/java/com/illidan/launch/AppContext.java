package com.illidan.launch;

import cn.hutool.core.util.ReUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Component;

/**
 * 程序上下文
 *
 * @author ben
 * @since 2022/5/16
 */
@Data
@Component
@ConditionalOnBean(ServerProperties.class)
public class AppContext {

  /** 应用程序名称 */
  private String appName;

  /** 短程序名称 */
  private String shortAppName;

  /** 环境 */
  private Environment env;

  /** 服务器属性 */
  private ServerProperties serverProperties;

  public AppContext() {
    appName = SpringUtil.getApplicationName();
    shortAppName = ReUtil.getGroup0("[^illidan-]+", appName);
    env = Environment.of(SpringUtil.getActiveProfile());
    serverProperties = SpringUtil.getBean(ServerProperties.class);
  }

  /**
   * 判断是否为开发环境
   *
   * @return boolean
   */
  public boolean isDev() {
    return env.equals(Environment.DEV);
  }
}
