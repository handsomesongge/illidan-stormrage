package com.illidan.launch;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.annotation.*;

/**
 * @author ben
 * @since 2022/5/16
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootTest
@ExtendWith(IllidanTestExtension.class)
public @interface IllidanTestApplication {

  /**
   * 应用程序名称
   *
   * @return {@link String}
   */
  String appName();

  /**
   * 测试主类
   *
   * @return {@link Class}<{@link ?}>
   */
  Class<?> clazz();

  /**
   * 环境
   *
   * @return {@link Environment}
   */
  Environment env() default Environment.DEV;
}
