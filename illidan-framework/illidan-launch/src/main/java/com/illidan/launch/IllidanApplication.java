package com.illidan.launch;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.retry.annotation.EnableRetry;

import java.lang.annotation.*;

/**
 * @author ben
 * @since 2022/5/16
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"com.illidan","com.illidan.system.feign","com.illidan.db.feign"})
@EnableRetry
public @interface IllidanApplication {}
