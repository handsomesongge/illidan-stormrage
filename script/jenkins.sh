# Jenkinsfile 配置参数
DOCKER_ACTION='run'
APP_NAME='illidan-stormrage'
ENVIRONMENT='master'
#
PROFILE=$ENVIRONMENT
if [ $ENVIRONMENT == 'master' ]; then
    PROFILE='pro'
fi
# 部署
if [ $DOCKER_ACTION == 'run' ]
then
  if [ $APP_NAME == 'illidan-stormrage' ]; then
#    获取所有前缀为illidan的镜像，生成固定格式写入文件
    docker images illidan** | awk '/'${PROFILE}-'/ {gsub(/'${PROFILE}'-/,"'${PROFILE}' -p ");position="-v /home/data/app/logs:/home/"$1"/logs --name="$1"-"$2" "$3" "$4":"$4" "$1":"$2"-"$4;print position}' | grep "$PROFILE" > jenkins-illidan.txt
#    遍历文件，一次读一行
    cat jenkins-illidan.txt | \
     while read line;
      do
#        运行镜像，映射日志目录 设置nacos订阅ip 设置运行环境
        docker run -itd -m 512m -v /etc/localtime:/etc/localtime:ro -e host=$(curl ifconfig.me) -e profiles=$PROFILE $line
      done
    rm -rf jenkins-illidan.txt
    else
#   运行镜像，映射日志目录 设置nacos订阅ip 设置运行环境
    docker_parameter=$(docker images $APP_NAME | awk '/'${PROFILE}-'/ {gsub(/'${PROFILE}-'/,"'${PROFILE}' -p ");position="--name="$1"-"$2" "$3" "$4":"$4" "$1":"$2"-"$4;print position}'| grep "$PROFILE")
    docker run -itd -m 512m  -v /etc/localtime:/etc/localtime:ro -v /home/data/app/logs:/home/$APP_NAME/logs -e host=$(curl ifconfig.me) -e profiles=$PROFILE $docker_parameter
  fi
  elif [ $DOCKER_ACTION == 'deploy' ]; then
     if [ $APP_NAME == 'illidan-stormrage' ]; then
          docker stop $(docker ps -a | grep illidan | grep $PROFILE | awk '{print $1}')
          docker rm $(docker ps -a | grep illidan | grep $PROFILE | awk '{print $1}')
      #    获取所有前缀为illidan的镜像，生成固定格式写入文件
          docker images illidan** | awk '/'${PROFILE}-'/ {gsub(/'${PROFILE}'-/,"'${PROFILE}' -p ");position="-v /home/data/app/logs:/home/"$1"/logs --name="$1"-"$2" "$3" "$4":"$4" "$1":"$2"-"$4;print position}' | grep "$PROFILE" > jenkins-illidan.txt
      #    遍历文件，一次读一行
          cat jenkins-illidan.txt | \
           while read line;
            do
      #        运行镜像，映射日志目录 设置nacos订阅ip 设置运行环境
              docker run -itd -m 512m -v /etc/localtime:/etc/localtime:ro -e host=$(curl ifconfig.me) -e profiles=$PROFILE $line
            done
          rm -rf jenkins-illidan.txt
          else
          docker stop $APP_NAME-$PROFILE
          docker rm $APP_NAME-$PROFILE
          docker_parameter=$(docker images $APP_NAME | awk '/'${PROFILE}-'/ {gsub(/'${PROFILE}-'/,"'${PROFILE}' -p ");position="--name="$1"-"$2" "$3" "$4":"$4" "$1":"$2"-"$4;print position}'| grep "$PROFILE")
          docker run -itd -m 512m  -v /etc/localtime:/etc/localtime:ro -v /home/data/app/logs:/home/$APP_NAME/logs -e host=$(curl ifconfig.me) -e profiles=$PROFILE $docker_parameter
        fi
#  销毁容器
  elif [ $DOCKER_ACTION == 'destroy' ]; then
     if [ $APP_NAME == 'illidan-stormrage' ]; then
          docker stop $(docker ps -a | grep illidan | grep $PROFILE | awk '{print $1}')
          docker rm $(docker ps -a | grep illidan | grep $PROFILE | awk '{print $1}')
          else
          docker stop $APP_NAME-$PROFILE
          docker rm $APP_NAME-$PROFILE
        fi
  else
#   删除 rm 开始 start 重开 restart 暂停 stop
    if [ $APP_NAME == 'illidan-stormrage' ]; then
      docker $DOCKER_ACTION $(docker ps -a | grep illidan | grep $PROFILE | awk '{print $1}')
      else
      docker $DOCKER_ACTION $APP_NAME-$PROFILE
    fi
fi