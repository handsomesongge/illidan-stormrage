package com.illidan.system.feign;

import com.illidan.launch.BaseAppName;
import com.illidan.system.entity.SysUser;
import com.illidan.tool.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

/**
 * @author ben
 * @since 2022/6/16
 */
@FeignClient(value = BaseAppName.ILLIDAN_SYSTEM, fallback = SysUserFeignFallback.class)
public interface ISysUserFeign {
  String BASE_API = "/user";

  /**
   * 根据用户名获取用户
   *
   * @param username 用户名
   * @return {@link R}<{@link SysUser}>
   */
  @GetMapping(BASE_API + "/username")
  R<SysUser> getUserByUsername(
      @Validated @NotNull(message = "用户名不能为空") @RequestParam String username);
}
