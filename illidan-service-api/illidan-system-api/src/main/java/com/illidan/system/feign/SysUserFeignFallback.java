package com.illidan.system.feign;

import com.illidan.system.entity.SysUser;
import com.illidan.tool.R;

/**
 * @author ben
 * @since 2022/6/16
 */
public class SysUserFeignFallback implements ISysUserFeign {
  @Override
  public R<SysUser> getUserByUsername(String username) {
    return R.fail("获取用户失败");
  }
}
