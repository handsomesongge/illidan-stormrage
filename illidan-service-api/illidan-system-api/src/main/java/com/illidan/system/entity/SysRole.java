package com.illidan.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.illidan.db.annotation.BindDict;
import com.illidan.db.model.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author ben
 * @since 2022/6/17
 */
@Data
@SuperBuilder
@NoArgsConstructor
@TableName("illidan_role")
public class SysRole extends BaseEntity {
  private Long parentId;
  private String name;
  private String code;
  @BindDict("status")
  private Integer status;
  private String statusText;
}
