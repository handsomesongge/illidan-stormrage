package com.illidan.system.vo;

import com.illidan.db.model.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

/**
 * @author ben
 * @since 2022/6/16
 */
@Data
@SuperBuilder
@NoArgsConstructor
@ApiModel("用户表视图")
public class SysUserVO extends BaseVO {
  @ApiModelProperty("用户名")
  @NotBlank(message = "用户名不能为空")
  private String username;

  @ApiModelProperty("密码")
  private String password;

  @ApiModelProperty("手机号码")
  private String mobile;

  @ApiModelProperty("昵称")
  @NotBlank(message = "昵称不能为空")
  private String nickname;

  @ApiModelProperty("头像")
  private String avatar;

  @ApiModelProperty("状态")
  private Integer status;

  @ApiModelProperty("状态文本")
  private String statusText;
}
