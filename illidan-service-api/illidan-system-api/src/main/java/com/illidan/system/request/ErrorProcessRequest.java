package com.illidan.system.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author ben
 * @since 2022/6/8
 */
@Data
@ApiModel("错误日志处理请求体")
public class ErrorProcessRequest {
    @NotNull(message = "错误日志主键不能为空")
    @ApiModelProperty("错误日志主键")
    private Long id;
    @ApiModelProperty("错误日志处理备注")
    private String remark;
}
