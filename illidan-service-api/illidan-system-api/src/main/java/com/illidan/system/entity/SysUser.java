package com.illidan.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.illidan.db.annotation.BindDict;
import com.illidan.db.model.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Set;

/**
 * @author ben
 * @since 2022/6/16
 */
@Data
@SuperBuilder
@NoArgsConstructor
@TableName("illidan_user")
public class SysUser extends BaseEntity {
  private String username;
  private String password;
  private String nickname;
  private String mobile;
  private String avatar;

  @BindDict("status")
  private Integer status;

  @TableField(exist = false)
  private String statusText;

  @TableField(exist = false)
  private Set<SysRole> roles;
}
