package com.illidan.common.launch;

import com.illidan.launch.Environment;
import com.illidan.launch.LaunchExtension;
import lombok.extern.slf4j.Slf4j;
import net.dreamlu.mica.auto.annotation.AutoService;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.util.Properties;

/**
 * 配置拓展
 *
 * @author ben
 * @since 2022/5/18
 */
@AutoService(LaunchExtension.class)
@Slf4j
public class CommonExtension implements LaunchExtension {

  public void extension(SpringApplicationBuilder builder, String appName, String env) {
    Properties properties = System.getProperties();
    // nacos 地址配置
    properties.setProperty(
        "spring.cloud.nacos.discovery.server-addr", NacosConstant.nacosAddr(env));
    properties.setProperty(
        "spring.cloud.nacos.discovery.namespace", NacosConstant.nacosNamespace(env));
    properties.setProperty("spring.cloud.nacos.config.server-addr", NacosConstant.nacosAddr(env));
    properties.setProperty(
        "spring.cloud.nacos.config.namespace", NacosConstant.nacosNamespace(env));
    properties.setProperty("spring.cloud.nacos.config.name", NacosConstant.NACOS_CONFIG_NAME);
    properties.setProperty("spring.cloud.nacos.config.file-extension", NacosConstant.NACOS_CONFIG_EXTENSION);
    // nacos 配置文件名称
    if (Environment.isDev(env)) {
      // nacos 打印获取配置
      properties.setProperty(
          "logging.level.com.alibaba.cloud.nacos.client.NacosPropertySourceBuilder", "debug");
    }
  }
}
