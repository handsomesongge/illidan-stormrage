package com.illidan.common.launch;

import com.illidan.launch.Environment;

/**
 * @author ben
 * @since 2022/5/18
 */
public interface NacosConstant {

  /** nacos 地址 */
  String NACOS_ADDR_DEV = "127.0.0.1:8848";

  String NACOS_ADDR_PRO = "175.6.208.149:8848";
  /** nacos 配置名称 */
  String NACOS_CONFIG_NAME = "illidan";
  /** nacos 配置拓展名 */
  String NACOS_CONFIG_EXTENSION = "yaml";
  /** nacos namespace */
  String NACOS_NAMESPACE_DEV = "83bb00ac-0f1a-4b46-94b0-c050fcc625be";

  String NACOS_NAMESPACE_PRO = "362497df-85ba-481a-a585-bf5c5870d24d";
  /** nacos share config */
  String NACOS_EXTENSION_CONFIG_DEFAULT_GROUP = "DEFAULT_GROUP";

  String NACOS_EXTENSION_CONFIG_DEFAULT_REFRESH = "true";

  /**
   * 获取nacos地址
   *
   * @param env env
   * @return {@link String}
   */
  static String nacosAddr(String env) {
    if (Environment.isDev(env)) {
      return NACOS_ADDR_DEV;
    }
    return NACOS_ADDR_PRO;
  }

  /**
   * 获取nacos命名空间
   *
   * @param env env
   * @return {@link String}
   */
  static String nacosNamespace(String env) {
    if (Environment.isDev(env)) {
      return NACOS_NAMESPACE_DEV;
    }
    return NACOS_NAMESPACE_PRO;
  }
}
