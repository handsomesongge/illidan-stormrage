package com.illidan.common.launch;

import com.illidan.launch.BaseAppName;

/**
 * @author ben
 * @since 2022/5/20
 */
public interface AppName {

	String ILLIDAN_DEMO = BaseAppName.NAME_PREFIX + "demo";

}
