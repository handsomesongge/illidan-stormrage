# illidan stormrage (伊利丹·怒风)

### 项目基础结构

```text
├── illidan-common                -- 常用通用工具
├── illidan-dependencies          -- 项目版本管理
├── illidan-basic                 -- 基础模块
    └── illidan-cloud                -- cloud模块
    └── illidan-core                 -- 核心模块
    └── illidan-db                   -- DB模块
    └── illidan-knife4j              -- knife4j模块
    └── illidan-launch               -- Boot容器模块
    └── illidan-nosql                -- nosql模块
└── illidan-tool                     -- 工具模块
├── illidan-gateway               -- 网关
├── illidan-service               -- 业务模块
    └── illidan-demo                 -- demo
├── illidan-service-api           -- 业务模块api封装
    └── illidan-system-api           -- 系统api
├── doc                           -- 项目相关文档
    └── maven                        -- maven setting文件
    └── mysql                        -- MySQL表结构和基础数据
    └── md                           -- MD文档
    └── nacos                        -- nacos配置文件
├── script                        -- 脚本
    
```

本程序仅供学习参考使用。严禁使用本程序进行违法行为或盈利行为! 