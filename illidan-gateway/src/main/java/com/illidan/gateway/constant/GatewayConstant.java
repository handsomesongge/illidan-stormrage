package com.illidan.gateway.constant;

/**
 * @author ben
 * @since 2022/5/23
 */
public interface GatewayConstant {

  String REQUEST_ID = "requestId";
}
