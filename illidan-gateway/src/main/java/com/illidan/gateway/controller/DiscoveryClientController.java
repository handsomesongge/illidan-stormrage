package com.illidan.gateway.controller;

import com.illidan.tool.$;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注册中心控制层
 *
 * @author ben
 * @since 2022/5/20
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/discovery")
public class DiscoveryClientController {

  private final DiscoveryClient discoveryClient;

  /**
   * 获取所有已注册的实例信息
   *
   * @return {@link Map}<{@link String}, {@link List}<{@link ServiceInstance}>>
   */
  @GetMapping("instances")
  public Map<String, List<ServiceInstance>> instances() {
    List<String> services = discoveryClient.getServices();
    return $.toMap(services, Function.identity(), discoveryClient::getInstances);
  }
}
