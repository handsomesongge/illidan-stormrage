package com.illidan.gateway;

import com.illidan.launch.BaseAppName;
import com.illidan.launch.BootApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ben
 * @since 2022/5/18
 */
@EnableDiscoveryClient
@SpringBootApplication
public class GatewayApplication {

  public static void main(String[] args) {
    BootApplication.run(BaseAppName.ILLIDAN_GATEWAY, GatewayApplication.class, args);
  }

}
