package com.illidan.gateway.extension;

import com.illidan.launch.LaunchExtension;
import net.dreamlu.mica.auto.annotation.AutoService;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.util.Properties;

/**
 * @author ben
 * @since 2022/5/23
 */
@AutoService(LaunchExtension.class)
public class GatewayExtension implements LaunchExtension {

  @Override
  public void extension(SpringApplicationBuilder builder, String appName, String env) {
    Properties properties = System.getProperties();
    properties.setProperty("spring.cloud.gateway.discovery.locator.enabled", "true");
    properties.setProperty("spring.cloud.loadbalancer.retry.enabled", "true");
    // shared config
    properties.setProperty(
        "spring.cloud.nacos.config.shared-configs[0].data-id", "illidan-gateway.yaml");
    properties.setProperty("spring.cloud.nacos.config.shared-configs[0].group", "DEFAULT_GROUP");
    properties.setProperty("spring.cloud.nacos.config.shared-configs[0].refresh", "true");
  }
}
