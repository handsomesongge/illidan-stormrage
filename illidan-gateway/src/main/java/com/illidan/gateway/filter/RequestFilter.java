package com.illidan.gateway.filter;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.addOriginalRequestUrl;

import cn.hutool.core.util.IdUtil;
import com.illidan.launch.NetUtil;
import java.util.Arrays;
import java.util.stream.Collectors;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author ben
 * @since 2022/5/23
 */
@Component
public class RequestFilter implements GlobalFilter, Ordered {

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    ServerHttpRequest request = exchange.getRequest();
    addOriginalRequestUrl(exchange, request.getURI());
    String rawPath = request.getURI().getRawPath();
    String newPath =
        "/"
            + Arrays.stream(StringUtils.tokenizeToStringArray(rawPath, "/"))
                .skip(1L)
                .collect(Collectors.joining("/"));
    ServerHttpRequest newRequest = request.mutate().path(newPath).build();
    exchange.getAttributes().put(GATEWAY_REQUEST_URL_ATTR, newRequest.getURI());
    return chain.filter(
        exchange
            .mutate()
            .request(
                newRequest.mutate().header(NetUtil.REQUEST_ID, IdUtil.fastSimpleUUID()).build())
            .build());
  }

  @Override
  public int getOrder() {
    return -1000;
  }
}
